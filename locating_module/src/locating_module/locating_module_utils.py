import numpy as np


def lin_interpolate(input_val, predefined_keys, predefined_values):
    last_index = len(predefined_keys) - 1

    for idx, key in enumerate(predefined_keys):
        if key == input_val:
            return predefined_values[idx]

        if idx < last_index and key <= input_val <= predefined_keys[idx + 1]:
            diff = predefined_keys[idx + 1] - key
            d_diff = input_val - key
            percent = d_diff / float(diff)
            dist_diff = predefined_values[idx + 1] - predefined_values[idx]
            return dist_diff * percent + predefined_values[idx]

        if input_val < key and idx == 0:
            return predefined_values[idx]

        if input_val > key and idx == last_index:
            return predefined_values[-1]

    return -1


"""
arg raw_obj_pos:

RawObjectPos

uint16 width
uint16 height
int16 x
uint16 y
uint16 y_top
uint16 y_bottom
uint16 x_left
uint16 x_right
uint32 area
uint16 depth
string type
"""


def get_nearest_edge_distance(center_pnt, raw_object_pos):
    p = raw_object_pos
    top_edge_dist = distance(center_pnt, np.array((p.x, p.y_top)))
    bottom_edge_dist = distance(center_pnt, np.array((p.x, p.y_bottom)))
    left_edge_dist = distance(center_pnt, np.array((p.x_left, p.y)))
    right_edge_dist = distance(center_pnt, np.array((p.x_right, p.y)))

    nearest = min([top_edge_dist, bottom_edge_dist, left_edge_dist, right_edge_dist])

    return nearest


def distance(p1, p2):
    return np.linalg.norm(p1 - p2)


def angle(a, b, c):
    ba = a - b
    bc = c - b

    cosine_angle = np.dot(ba, bc) / (np.linalg.norm(ba) * np.linalg.norm(bc))

    ang = np.arccos(cosine_angle)
    degrees = np.degrees(ang)

    return degrees
