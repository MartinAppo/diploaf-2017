#!/usr/bin/env python


from image_processing.msg import RawObjectPos
from image_processing.msg import RawObjectPositions
from locating_module.msg import ObjectPositions
import rospy


class TranslateCoords():
    def __init__(self, raw_obj_pos_topic, publish_topic):
        self.running = False
        self.raw_object_positions = RawObjectPositions()
        self.raw_balls_pos = []
        self.raw_own_gate_pos = RawObjectPos()
        self.raw_opponent_gate_pos = RawObjectPos()
        self.origin = "Unknown"
        self.publisher = rospy.Publisher(publish_topic, ObjectPositions, queue_size=1)
        rospy.Subscriber(raw_obj_pos_topic, RawObjectPositions,
                         self.object_positions_callback)

    def object_positions_callback(self, object_positions_msg):
        if not self.running:
            return

        self.raw_object_positions = object_positions_msg
        self.raw_own_gate_pos = object_positions_msg.OwnGatePos
        self.raw_opponent_gate_pos = object_positions_msg.OpponentGatePos
        self.raw_balls_pos = object_positions_msg.Balls
        self.publish_obj_positions()

    def publish_obj_positions(self):
        if self.ready_to_publish():
            object_positions = self.get_object_positions()
            self.publisher.publish(object_positions)

    def get_object_positions(self):
        object_positions = ObjectPositions()
        own_gate_pos = self.convert_raw_to_real(self.raw_own_gate_pos)
        opponent_gate_pos = self.convert_raw_to_real(self.raw_opponent_gate_pos)
        balls_positions = []
        for raw_ball_pos in self.raw_balls_pos:
            balls_positions.append(self.convert_raw_to_real(raw_ball_pos))

        object_positions.Origin = self.origin
        object_positions.OwnGatePos = own_gate_pos
        object_positions.OpponentGatePos = opponent_gate_pos
        object_positions.Balls = balls_positions

        return object_positions

    def convert_raw_to_real(self, raw_object_pos):
        # Override in superclass
        return ObjectPositions()

    def ready_to_publish(self):
        # Override in superclass
        return True