#!/usr/bin/env python

import rospy
import diploaf_utils.math_util as math_util
from locating_module.srv import DriveTo, DriveToRequest, DriveToResponse
from locating_module.srv import ResetOdom
from locating_module.srv import ResetOdomRequest
from geometry_msgs.msg import Twist
from std_msgs.msg import Int16
from nav_msgs.msg import Odometry
from std_srvs.srv import Empty, EmptyRequest, EmptyResponse
import tf
import tf_conversions
from math import *
from geometry_msgs.msg import Vector3
from time import time


class RobotDriverNode():
    def __init__(self):
        rospy.init_node("robot_driver_node", anonymous=True)
        self.rate = rospy.Rate(60)
        self.target_pos = None
        self.odom_position = None  # robot cartesian position from last odometry reset position
        self.odom_orientation = None
        self.odom_turned_deg = 0
        self.odom_target_pos = None
        self.cancelled = False
        self.current_twist_msg = Twist()
        self.max_wheel_speed = rospy.get_param("max_linear_speed")
        self.min_wheel_speed = rospy.get_param("min_wheel_speed")

        rospy.Service("robot_driver_node/drive_to", DriveTo, self.drive_to)
        rospy.Service("robot_driver_node/turn_to", DriveTo, self.turn_to)
        rospy.Service("robot_driver_node/cancel", Empty, self.cancel)
        rospy.Subscriber("locating_module/odom", Odometry, self.odometry_callback)
        rospy.Subscriber("locating_module/odom_turned_degrees", Int16, self.turned_degrees_callback)
        self.cmd_vel_publisher = rospy.Publisher("logic_module/cmd_vel", Twist, queue_size=1)
        self.linear_vel_pub = rospy.Publisher("logic_module/linear_vel", Vector3, queue_size=1)
        self.angular_vel_pub = rospy.Publisher("logic_module/angular_vel", Vector3, queue_size=1)
        self.call_reset_odom = rospy.ServiceProxy("reset_odom", ResetOdom)
        self.last_distance_from_target = None

    def init_service_call(self, drive_to):
        self.last_distance_from_target = None
        self.target_pos = drive_to.targetPos
        rospy.wait_for_service("reset_odom")
        self.call_reset_odom(ResetOdomRequest(self.target_pos, None))

        self.current_twist_msg.angular.x = 0.0  # TODO angular velocity? segment angular and linear velocities to different topics?
        self.current_twist_msg.linear = Twist().linear
        self.current_twist_msg.linear.y = self.target_pos.y
        self.current_twist_msg.linear.x = self.target_pos.x

    def drive_to(self, drive_to):
        self.cancelled = False
        self.init_service_call(drive_to)

        while not rospy.is_shutdown():
            if self.cancelled:
                break

            if self.odom_position is None:
                continue

            distance = math_util.get_distance_between_two_vectors(self.odom_position, self.target_pos)
            if self.last_distance_from_target is not None and distance - self.last_distance_from_target > 0.01:
                rospy.logwarn("drive_to service stopped, the robot was actually getting further from target!!")
                rospy.logwarn("calc distance {}".format(distance))
                rospy.logwarn("last distance {}".format(self.last_distance_from_target))
                break

            self.last_distance_from_target = distance
            speed = self.calc_robot_speed_by_distance(distance)
            dir_vector = math_util.get_vector_between_two_vectors(self.odom_position, self.target_pos)
            corrected_moving_vector = math_util.change_vector_length_and_dir(dir_vector, speed, self.odom_turned_deg)

            self.current_twist_msg.linear.y = corrected_moving_vector.y
            self.current_twist_msg.linear.x = corrected_moving_vector.x
            self.linear_vel_pub.publish(self.current_twist_msg.linear)

            self.rate.sleep()
            if distance <= 0.006:
                break

        self.reset()
        self.stop()

        return DriveToResponse()

    def stop(self):
        for i in range(100):
            self.current_twist_msg.angular.x = 0
            self.current_twist_msg.linear = Twist().linear
            self.cmd_vel_publisher.publish(self.current_twist_msg)

    def cancel(self, args):
        self.cancelled = True

        return EmptyResponse()

    def turn_to(self, drive_to):
        self.cancelled = False
        self.init_service_call(drive_to)

        target_angle = drive_to.targetPos.z

        while not rospy.is_shutdown():
            if self.cancelled:
                break

            if self.odom_orientation is None:
                continue

            offset = abs(target_angle) - abs(self.odom_turned_deg)
            self.current_twist_msg.linear.y = 0
            self.current_twist_msg.linear.x = 0
            self.current_twist_msg.angular.x = copysign(max(2, offset / 25), target_angle)
            self.angular_vel_pub.publish(self.current_twist_msg.angular)

            rospy.loginfo_throttle(0.3, "target drive: {}".format(target_angle))
            rospy.loginfo_throttle(0.3, "odom orient:  {}".format(self.odom_turned_deg))
            if offset <= 0:
                break
            self.rate.sleep()

        self.stop()
        self.reset()

        return DriveToResponse()

    def reset(self):
        self.target_pos = None
        self.odom_position = None
        self.odom_orientation = None
        self.odom_turned_deg = 0
        self.current_twist_msg = Twist()
        self.last_distance_from_target = None
        rospy.wait_for_service("reset_odom")
        self.call_reset_odom(ResetOdomRequest(self.target_pos, None))

    def calc_robot_speed_by_distance(self, distance):
        if distance >= 1.0:
            return self.max_wheel_speed
        else:
            return max((distance * 1.1) + 0.2, self.min_wheel_speed)  # TODO kas siin on mingit targemat loogikat vaja

    def run(self):
        rate = rospy.Rate(60)
        while not rospy.is_shutdown():
            rate.sleep()

    def odometry_callback(self, odometry_msg):
        (roll, pitch, yaw) = tf_conversions.transformations.euler_from_quaternion(
            [odometry_msg.pose.pose.orientation.x, odometry_msg.pose.pose.orientation.y,
             odometry_msg.pose.pose.orientation.z, odometry_msg.pose.pose.orientation.w])

        self.odom_orientation = yaw * 180 / pi
        self.odom_position = odometry_msg.pose.pose.position

    def turned_degrees_callback(self, int_msg):
        self.odom_turned_deg = int_msg.data


if __name__ == '__main__':
    try:
        RobotDriverNode().run()
    except rospy.ROSInterruptException as e:
        pass
