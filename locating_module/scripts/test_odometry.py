#!/usr/bin/env python

import rospy
from locating_module.srv import DriveTo, DriveToRequest, DriveToResponse
from geometry_msgs.msg import Vector3
from locating_module.srv import ResetOdom
from locating_module.srv import ResetOdomRequest
import diploaf_utils.math_util as math_util

if __name__ == '__main__':
    rospy.init_node("test_odometry", anonymous=True)

    #call_drive_to = rospy.ServiceProxy("robot_driver_node/drive_to", DriveTo)
    #rospy.wait_for_service("robot_driver_node/drive_to")
    #call_drive_to(DriveToRequest(Vector3(2, -3, 0)))

    call_drive_to = rospy.ServiceProxy("robot_driver_node/turn_to", DriveTo)
    rospy.wait_for_service("robot_driver_node/turn_to")
    call_drive_to(DriveToRequest(Vector3(0, 0, 180)))

    # call_reset_odom = rospy.ServiceProxy("reset_odom", ResetOdom)
    # rospy.wait_for_service("reset_odom")
    # call_reset_odom(ResetOdomRequest(Vector3(0, 1, 0), None))
