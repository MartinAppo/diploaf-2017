#! /usr/bin/env python

import rospy
import diploaf_robot.config_client as config_client
from hardware_module.msg import WheelSpeeds
from nav_msgs.msg import Odometry
from locating_module.srv import ResetOdom
from geometry_msgs.msg import Point, Pose, Quaternion, Twist, Vector3
from locating_module.msg import ObjectPos
from std_msgs.msg import Int16
from math import *
import diploaf_utils.math_util as math_util
import tf
import tf_conversions
import numpy as np


class OdometryNode(config_client.ConfigClient):
    def __init__(self):
        rospy.init_node("odometry", anonymous=True)
        config_client.ConfigClient.__init__(self)

        self.wheel_positions = rospy.get_param("/wheel_positions")
        self.wheel_directions = rospy.get_param("/wheel_directions")

        self.DEG_45 = pi / 4
        self.DEG_135 = 0.75 * pi
        self.original_angles = [-self.DEG_45, self.DEG_45, self.DEG_135, -self.DEG_135]
        self.angles = [self.original_angles[i] for i in self.wheel_positions]

        self.wheel_distance_from_center = rospy.get_param("/wheel_distance_from_center")  # in m
        self.wheel_radius = rospy.get_param("/wheel_radius")  # in m
        self.gear_ratio = rospy.get_param("/gear_ratio")

        self.wheel_radius_inv = 1.0 / self.wheel_radius
        self.gear_ratio_inv = 1.0 / self.gear_ratio
        self.rad_to_rpm = 0.10472

        self.last_timestamp = None
        self.dt = None

        self.omega_matrix_inv_a = np.matrix(
            [[sin(self.angles[0]), cos(self.angles[0]), self.wheel_distance_from_center],
             [sin(self.angles[1]), cos(self.angles[1]), self.wheel_distance_from_center],
             [sin(self.angles[2]), cos(self.angles[2]), self.wheel_distance_from_center]]).getI()

        # moved
        self.x = 0.0
        self.y = 0.0
        self.th = 0.0
        self.th_deg = 0.0

        # target
        self.target = None
        self.ball_pos = None

        rospy.Subscriber("hardware_module/motor_speed_feedback", WheelSpeeds, self.motor_speed_feedback_callback)
        self.odom_pub = rospy.Publisher("locating_module/odom", Odometry, queue_size=50)
        self.target_pos_pub = rospy.Publisher("locating_module/odom_target_position", Vector3, queue_size=1)
        self.odom_ball_pub = rospy.Publisher("locating_module/odom_ball_position", ObjectPos, queue_size=1)
        self.odom_turn_pub = rospy.Publisher("locating_module/odom_turned_degrees", Int16, queue_size=1)
        self.odom_broadcaster = tf.TransformBroadcaster()
        rospy.Service("reset_odom", ResetOdom, self.reset_odom)
        rospy.Service("reset_odom_ball", ResetOdom, self.reset_odom_ball)

    # TODO call this continuously in command receiver
    def reset_odom(self, reset_odom_request):
        self.x = 0.0
        self.y = 0.0
        self.th = 0.0
        self.th_deg = 0.0
        #rospy.loginfo("reset odom args: {}".format(reset_odom_request))
        self.target = reset_odom_request.targetPos
        return []

    def reset_odom_ball(self, reset_odom_request):
        self.ball_pos = reset_odom_request.ballSnapshot

        return []

    def motor_speed_feedback_callback(self, motor_rpms_msg):
        motor_rpms = motor_rpms_msg.wheelSpeeds

        wheel_matrix_a = np.matrix([[self.wheel_radius * -1.0 * self.rad_to_rpm * self.gear_ratio_inv * motor_rpms[
            self.wheel_positions[0]] * self.wheel_directions[self.wheel_positions[0]]],
                                    [self.wheel_radius * -1.0 * self.rad_to_rpm * self.gear_ratio_inv * motor_rpms[
                                        self.wheel_positions[1]] * self.wheel_directions[self.wheel_positions[1]]],
                                    [self.wheel_radius * -1.0 * self.rad_to_rpm * self.gear_ratio_inv * motor_rpms[
                                        self.wheel_positions[2]] * self.wheel_directions[self.wheel_positions[2]]]])
        solved_robot_speed = self.omega_matrix_inv_a * wheel_matrix_a
        solved_robot_speed = np.squeeze(np.asarray(solved_robot_speed))

        twist = Twist()
        twist.linear.x = -solved_robot_speed[0]
        twist.linear.y = -solved_robot_speed[1]
        twist.angular.x = solved_robot_speed[2]

        if self.last_timestamp is not None:
            self.dt = motor_rpms_msg.header.stamp - self.last_timestamp

        if self.dt is not None:
            dt_sec = self.dt.to_sec()

            delta_dt = twist.angular.x * dt_sec
            self.th += delta_dt
            self.th_deg += delta_dt * 180 / pi
            self.odom_turn_pub.publish(Int16(self.th_deg))

            # rospy.loginfo_throttle(0.3, "th: {}".format(self.th_deg))

            delta_x = (twist.linear.x * cos(self.th) - twist.linear.y * sin(self.th)) * dt_sec
            delta_y = (twist.linear.x * sin(self.th) + twist.linear.y * cos(self.th)) * dt_sec

            self.x += delta_x
            self.y += delta_y

            # rospy.loginfo_throttle(1, "moved x: {}".format(self.x))
            # rospy.loginfo_throttle(1, "moved y: {}".format(self.y))
            # rospy.loginfo_throttle(1, "moved th: {}".format(self.th))

            odom_quat = tf_conversions.transformations.quaternion_from_euler(0, 0, self.th)
            static_quat = tf_conversions.transformations.quaternion_from_euler(0, 0, 0)

            self.odom_broadcaster.sendTransform(
                (self.x, self.y, 0),
                odom_quat,
                motor_rpms_msg.header.stamp,
                "odom",
                "world"
            )

            if self.target is not None:
                self.odom_broadcaster.sendTransform(
                    (self.target.x, self.target.y, 0),
                    static_quat,
                    motor_rpms_msg.header.stamp,
                    "odom_target",
                    "world"
                )

                self.target_pos_pub.publish(Vector3(self.target.x, self.target.y, 0))


            if self.ball_pos is not None:
                self.ball_pos.CartesianPos.x -= delta_x
                self.ball_pos.CartesianPos.y -= delta_y
                dir_and_len = math_util.vector_to_direction_and_length(self.ball_pos.CartesianPos)
                angle = dir_and_len['dir'] + self.th_deg
                self.ball_pos.Distance = dir_and_len['len']
                self.ball_pos.Angle = angle
                self.ball_pos.Angle180 = angle if angle <= 180.0 else angle - 360.0

                ball_vector = math_util.direction_and_length_to_vector(self.ball_pos.Angle, self.ball_pos.Distance)

                self.odom_broadcaster.sendTransform(
                    (ball_vector.x, ball_vector.y, 0),
                    static_quat,
                    motor_rpms_msg.header.stamp,
                    "odom_ball",
                    "realsense_camera"  # TODO martin - choose which camera
                )

                self.odom_ball_pub.publish(self.ball_pos)

            odometry_msg = Odometry()
            odometry_msg.header.stamp = motor_rpms_msg.header.stamp
            odometry_msg.header.frame_id = "odom"
            odometry_msg.child_frame_id = "base_link"
            odometry_msg.pose.pose = Pose(Point(self.x, self.y, 0.0), Quaternion(*odom_quat))

            self.odom_pub.publish(odometry_msg)

        self.last_timestamp = motor_rpms_msg.header.stamp

    def run(self):
        rospy.Rate(60)
        rospy.spin()


if __name__ == '__main__':
    try:
        OdometryNode().run()
    except rospy.ROSInterruptException:
        pass
