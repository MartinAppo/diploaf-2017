#!/usr/bin/env python


import json
import image_processing.ximea_conf as conf
from locating_module.msg import ObjectPos
from locating_module.translate_coords import TranslateCoords
import locating_module.locating_module_utils as utils
from geometry_msgs.msg import Point, Vector3
from std_msgs.msg import Int16
import diploaf_robot.config_client as config_client
import rospy
import numpy as np
import diploaf_utils.math_util as math_util


class XimeaTranslateCoords(TranslateCoords, config_client.ConfigClient):
    def __init__(self):
        config_client.ConfigClient.__init__(self)
        self.init_config_client()
        TranslateCoords.__init__(self, "image_processing/ximea_raw_object_positions",
                                 "locating_module/ximea/object_positions")
        with open(conf.XIMEA_DISTANCES_CONF_PATH, 'rb') as json_file:
            self.raw_distances_lower, self.distances = json.loads(json_file.read())

        rospy.loginfo("Ximea distances calibration:")
        rospy.loginfo("Raw lower: {}".format(self.raw_distances_lower))
        rospy.loginfo("Real distances in m: {}".format(self.distances))

        # Field obstruction
        rospy.Subscriber("image_processing/field_obstruction", Point, self.field_obstruction_callback)
        self.field_obstruction_pub = rospy.Publisher("locating_module/field_obstruction_angle", Int16,
                                                     queue_size=1)
        self.origin = "ximea"
        self.running = True

    def convert_raw_to_real(self, raw_object_pos):
        object_pos = ObjectPos()
        if raw_object_pos.x == -1:
            object_pos.Distance = -1
        else:
            raw_distance_nearest = utils.get_nearest_edge_distance(conf.MID_POS,
                                                                                                   raw_object_pos)
            object_pos.Distance = utils.lin_interpolate(raw_distance_nearest, self.raw_distances_lower, self.distances)
            angle = utils.angle(np.array((raw_object_pos.x, raw_object_pos.y)), conf.MID_POS, conf.MID_LEFT_POS)

            object_pos.Angle = angle if raw_object_pos.y > conf.MID_Y else 360 - angle
            object_pos.Angle180 = angle if raw_object_pos.y > conf.MID_Y else - angle
            object_pos.Width = raw_object_pos.width
            object_pos.Type = raw_object_pos.type
            x, y = math_util.pol2cart(object_pos.Distance, object_pos.Angle)
            object_pos.CartesianPos = Vector3(x, y, 0)
        return object_pos

    def field_obstruction_callback(self, field_obstr_pnt):
        field_obstr_angle = utils.angle(np.array((field_obstr_pnt.x, field_obstr_pnt.y)), conf.MID_POS,
                                        conf.MID_LEFT_POS)
        self.field_obstruction_pub.publish(
            field_obstr_angle if field_obstr_pnt.y > conf.MID_Y else 360 - field_obstr_angle)

    def ready_to_publish(self):
        return self.config['ximea_enabled']


if __name__ == '__main__':
    try:
        rospy.init_node("ximea_translate_coords", anonymous=True)
        rate = rospy.Rate(60)

        XimeaTranslateCoords()

        while not rospy.is_shutdown():
            rate.sleep()

    except rospy.ROSInterruptException as e:
        pass
