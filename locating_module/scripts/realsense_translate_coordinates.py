#!/usr/bin/env python


from locating_module.msg import ObjectPos
from locating_module.translate_coords import TranslateCoords
import diploaf_robot.config_client as config_client
import rospy
import tf
import diploaf_utils.math_util as math_util
from geometry_msgs.msg import Vector3
import tf_conversions
import json
import image_processing.front_cam_conf as conf


class RealsenseTranslateCoords(TranslateCoords, config_client.ConfigClient):
    def __init__(self):
        config_client.ConfigClient.__init__(self)
        TranslateCoords.__init__(self, "image_processing/realsense_raw_object_positions",
                                 "locating_module/realsense/object_positions")
        self.init_config_client()
        self.FOV = rospy.get_param("/front_cam_fov")
        self.horizontal_len = rospy.get_param("/front_cam_horizontal_len")
        self.origin = "front"
        self.tf_broadcaster = tf.TransformBroadcaster()
        with open(conf.DISTANCES_CONF_PATH, 'r') as json_file:
            self.y_coords, self.distances, self.Y_MAX_DISTANCE = json.loads(json_file.read())
        self.running = True

    def convert_raw_to_real(self, raw_object_pos):
        object_pos = ObjectPos()
        if raw_object_pos.x == -1:
            object_pos.Distance = -1
            object_pos.Distance = -1
        else:
            left_right_ratio = raw_object_pos.x / self.horizontal_len  # 0 means that the object is totally left, 1 means it is totally right in the picture
            if raw_object_pos.depth == 0.0: #depth sensor did not find distance
                object_pos.Distance = self.lin_interpolate(raw_object_pos.y_bottom)
                #rospy.loginfo_throttle(1, "no distance from sensor. calculated: {}".format(object_pos.Distance))
            else:
                object_pos.Distance = raw_object_pos.depth / 1000.0 #Because realsense returns in mm, we want m

            object_pos.Angle180 = (left_right_ratio - 0.5) * self.FOV
            object_pos.Angle = 360.0 + object_pos.Angle180 if left_right_ratio < 0.5 else object_pos.Angle180
            object_pos.Width = raw_object_pos.width
            object_pos.Type = raw_object_pos.type
            x, y = math_util.pol2cart(object_pos.Distance, object_pos.Angle)
            object_pos.CartesianPos = Vector3(x, y, 0)
            self.broadcast_object(object_pos) #TODO only in debug mode?
        return object_pos

    def broadcast_object(self, object_pos):
        vector = object_pos.CartesianPos
        if object_pos.Type == "ball":
            self.tf_broadcaster.sendTransform((vector.x, vector.y, 0),
                                              tf_conversions.transformations.quaternion_from_euler(0, 0, 0),
                                              rospy.Time.now(),
                                              "single_realsense_ball",
                                              "realsense_camera")
        if object_pos.Type == "target":
            self.tf_broadcaster.sendTransform((vector.x, vector.y, 0),
                                              tf_conversions.transformations.quaternion_from_euler(0, 0, 0),
                                              rospy.Time.now(),
                                              "realsense_target",
                                              "realsense_camera")

    def lin_interpolate(self, y):
        last_index = len(self.y_coords) - 1
        for idx, y_coord in enumerate(self.y_coords):
            if y_coord == y:
                return self.distances[idx]

            if idx < last_index and y_coord >= y >= self.y_coords[idx + 1]:
                y_coord_diff = y_coord - self.y_coords[idx + 1]
                y_diff = y_coord - y
                percent = y_diff / float(y_coord_diff)
                dist_diff = self.distances[idx + 1] - self.distances[idx]
                return dist_diff * percent + self.distances[idx]

            if y > y_coord and idx == 0:
                return self.distances[idx]

            if y < y_coord and idx == last_index:
                return self.distances[-1]

        return -1

    def ready_to_publish(self):
        return self.config['front_cam_enabled']


if __name__ == '__main__':
    try:
        rospy.init_node("realsense_translate_coords", anonymous=True)
        rate = rospy.Rate(60)

        RealsenseTranslateCoords()

        while not rospy.is_shutdown():
            rate.sleep()

    except rospy.ROSInterruptException as e:
        pass
