#!/usr/bin/env python

import cv2
import numpy as np
import cPickle as pickle
import image_processing.front_cam_conf as conf
from image_processing.msg import RawObjectPos
from image_processing.msg import RawObjectPositions
from diploaf_utils.key_listener import KeyListener
from diploaf_robot.msg import KeyValue
import rospy
import json


class DistanceCalib(KeyListener):
    def __init__(self):
        rospy.init_node("realsense_distance_calibration", anonymous=True)
        KeyListener.__init__(self)
        rospy.Subscriber("image_processing/realsense_raw_object_positions", RawObjectPositions,
                         self.object_positions_callback)
        self.gui_publisher = rospy.Publisher("rqt_diploaf_dashboard/status", KeyValue, queue_size=1)
        self.calib_mode = rospy.get_param("/front_cam_dist_calib_mode") #ball or target
        self.counter = 0
        self.y_med = 0
        self.y_max = 0
        self.y_med_list = []
        self.ys = []
        self.i = 0
        self.distances = np.array([0.10, 0.13, 0.16, 0.21, 0.26]) #only close distances, further ones are calculated with depth sensor
        rospy.Timer(rospy.Duration(1), self.monitoring_pub_callback)
        self.current_message = ""

    def monitoring_pub_callback(self, args):
        self.publish_gui_status()

    def publish_gui_status(self):
        msg = KeyValue()
        msg.MsgType = "realsense_dist_calib"
        msg.MsgKey = "distance_calib"
        msg.MsgValue = self.current_message
        self.gui_publisher.publish(msg)

    def object_positions_callback(self, object_pos_msg):
        if self.calib_mode == "ball":
            object_pos = self.get_nearest(object_pos_msg.Balls)
        elif self.calib_mode == "target":
            object_pos = object_pos_msg.OpponentGatePos
        else:
            rospy.logwarn("No object found!")
            return

        if object_pos is not None and object_pos.x != -1:
            if self.counter >= 30:
                self.y_med = np.median(self.y_med_list)
                self.counter = 0
                self.y_med_list = []
            else:
                self.y_med_list.append(object_pos.y_bottom)
                self.counter += 1

    # Only for using with balls
    def get_nearest(self, object_positions):
        nearest = None
        for pos in object_positions:
            if nearest is None or pos.y_bottom > nearest.y_bottom:
                nearest = pos
        return nearest

    def key_event_callback(self, key_event_msg):
        if key_event_msg.pressed:
            return

        if key_event_msg.char == "b":  # back
            self.clear_last()
            return

        if key_event_msg.char == "r":  # reset
            self.clear_all()
            return

        if key_event_msg.char == "c":  # current
            self.save_current()
            return

        if key_event_msg.char == "s":  # save
            self.save_all()
            return

    def clear_last(self):
        if len(self.ys) < 1:
            return

        self.ys.pop()
        self.i -= 1
        self.current_message = "Last cleared. Press c when object is {} m from camera".format(self.distances[self.i])

    def clear_all(self):
        self.i = 0
        self.ys = []
        self.current_message = "Distances cleared. Press c when object is {} m from camera".format(
            self.distances[self.i])

    def save_current(self):
        if len(self.ys) == len(self.distances):
            self.current_message = "All distances marked. Please save all with 's'"
            return

        rospy.loginfo("y: {}".format(self.y_med))
        self.ys.append(self.y_med)

        self.i += 1
        if self.i == len(self.distances):
            self.y_max = self.y_med

        if len(self.ys) == len(self.distances):
            self.current_message = "All distances marked. Please save all with 's'"
            return

        self.current_message = "Saved {}m:{}px. Press c when object is {} m from camera".format(
            self.distances[self.i - 1], self.y_med, self.distances[self.i])

    def save_all(self):
        y_coords = np.array(self.ys)
        with open(conf.DISTANCES_CONF_PATH, 'w') as json_file:
            json_file.write(json.dumps([y_coords.tolist(), self.distances.tolist(), self.y_max]))

        if len(self.ys) == len(self.distances):
            self.current_message = "All distances saved"
        else:
            self.current_message = "Some distances saved"

    def run(self):
        self.current_message = "Press c when object is {} m from the camera".format(self.distances[self.i])

        rate = rospy.Rate(60)
        while not rospy.is_shutdown():
            rate.sleep()


if __name__ == '__main__':
    try:
        calib = DistanceCalib()
        calib.run()

    except rospy.ROSInterruptException as e:
        pass
