#!/usr/bin/env python

import numpy as np
import image_processing.ximea_conf as conf
from image_processing.msg import RawObjectPositions
from locating_module.msg import ObjectPositions
from diploaf_utils.key_listener import KeyListener
from sensor_msgs.msg import CompressedImage
import locating_module.locating_module_utils as utils
from locating_module.srv import DriveTo, DriveToRequest, DriveToResponse
from geometry_msgs.msg import Vector3
import diploaf_utils.math_util as math_util
import rospy
import json


class XimeaDistanceCalib(KeyListener):
    def __init__(self):
        KeyListener.__init__(self)
        self.height = conf.HEIGHT
        self.width = conf.WIDTH
        self.frame = None

        self.mid_y = conf.MID_Y
        self.mid_x = conf.MID_X
        self.mid_pos = conf.MID_POS

        self.opponent_gate_pos = None
        self.reference_distances = [0.17, 0.30, 0.45, 0.60, 0.80, 1.00, 1.30, 1.60, 1.90, 2.20, 2.50, 2.80, 3.10, 3.40, 4.00, 4.50, 5.00, 5.50] #in m
        self.actual_distances = []
        self.distances_counter = 0
        self.saved_current_pos_count = 0
        self.saved_current_distances_lower = []
        self.callback_counter = 0
        self.actual_distance = None
        self.distances_med = []

        try:
            with open(conf.XIMEA_DISTANCES_CONF_PATH, 'r') as json_file:
                self.raw_distances_lower, loaded_distances = json.loads(json_file.read())
                self.distances_counter = len(self.raw_distances_lower)
                rospy.loginfo("Loaded distances: {}".format(loaded_distances))
                rospy.loginfo("Loaded raw lower: {}".format(self.raw_distances_lower))
                rospy.loginfo("Distances counter: {}".format(self.distances_counter))
        except Exception as e:
            self.raw_distances_lower = []

        self.image_pub = rospy.Publisher("locating_module/ximea_debug_img/compressed", CompressedImage, queue_size=1)
        rospy.Subscriber("image_processing/ximea_raw_object_positions", RawObjectPositions,
                         self.object_positions_callback)
        self.obj_sub = rospy.Subscriber("locating_module/realsense/object_positions", ObjectPositions,
                                        self.rs_object_positions_callback)
        self.call_drive_to = rospy.ServiceProxy("robot_driver_node/drive_to", DriveTo)

        self.save_in_progress = False

        rospy.loginfo("To save a distance, press c (current)")
        rospy.loginfo("To revert a distance, press b (back)")
        rospy.loginfo("To reset all conf, press r (reset)")
        rospy.loginfo("To save conf, press s (save)")
        rospy.loginfo("====== Start calibration ======")

        self.notify_new_distane()

    def rs_object_positions_callback(self, rs_object_msg):
        if self.callback_counter >= 30:
            self.actual_distance = np.median(self.distances_med)
            rospy.loginfo_throttle(1, "Current actual distance: {}".format(self.actual_distance))
            self.callback_counter = 0
            self.distances_med = []
        else:
            self.distances_med.append(rs_object_msg.OpponentGatePos.Distance)
            self.callback_counter += 1

    def object_positions_callback(self, object_pos_msg):
        if self.save_in_progress:
            self.save_current()

        if object_pos_msg.OpponentGatePos.x != 0 or object_pos_msg.OpponentGatePos.y != 0:
            self.opponent_gate_pos = object_pos_msg.OpponentGatePos
        else:
            self.opponent_gate_pos = None

    def key_event_callback(self, key_event_msg):
        if key_event_msg.pressed:
            return

        if key_event_msg.char == "b":  # back
            self.clear_last()
            return

        if key_event_msg.char == "r":  # reset
            self.clear_all()
            return

        if key_event_msg.char == "c":  # current
            self.save_current()
            return

        if key_event_msg.char == "s":  # save
            self.save_all()
            return

    def save_current(self):
        if self.distances_counter >= len(self.reference_distances):
            rospy.loginfo(
                "All {} distances have been saved. You can press 's' to save now.".format(self.distances_counter))
            return

        if self.save_in_progress:
            if self.saved_current_pos_count >= 60:
                if len(self.saved_current_distances_lower) == 0:
                    rospy.logwarn("No OpponentGate found!")
                    self.save_in_progress = False
                    return
                else:
                    distance_nearest = np.median(self.saved_current_distances_lower)
                    self.raw_distances_lower.append(distance_nearest)
                    self.actual_distances.append(self.actual_distance)
                    rospy.loginfo("Saved distance nearest: {}px".format(distance_nearest))
                    rospy.loginfo("Saved actual distance:  {}m".format(self.actual_distance))

                    #self.reverse_to_next()

                    self.distances_counter += 1
                    self.notify_new_distane()

                    self.save_in_progress = False
                    self.saved_current_distances_lower = []
                    self.saved_current_pos_count = 0

            else:
                rospy.loginfo(utils.get_nearest_edge_distance(conf.MID_POS, self.opponent_gate_pos))
                self.saved_current_pos_count += 1
                nearest = utils.get_nearest_edge_distance(conf.MID_POS, self.opponent_gate_pos)
                self.saved_current_distances_lower.append(nearest)
        else:
            self.save_in_progress = True

    def reverse_to_next(self):
        if self.distances_counter+1 >= len(self.reference_distances):
            rospy.loginfo(
                "All {} distances have been saved. You can press 's' to save now.".format(self.distances_counter))
            return

        rospy.wait_for_service("robot_driver_node/drive_to")
        meters_to_reverse = self.reference_distances[self.distances_counter] - self.reference_distances[
            self.distances_counter + 1]
        rospy.loginfo("Reversing {}m".format(meters_to_reverse))
        vect = math_util.direction_and_length_to_vector(0, -meters_to_reverse)
        self.call_drive_to(DriveToRequest(Vector3(vect.x, vect.y, 0)))

    def save_all(self):
        with open(conf.XIMEA_DISTANCES_CONF_PATH, 'w') as json_file:
            json_file.write(json.dumps([self.raw_distances_lower, self.actual_distances]))

        rospy.loginfo(
            "Saved. \n distances (m): {} \n raw (pixels): {}".format(self.actual_distances, self.raw_distances_lower))

    def clear_last(self):
        if self.distances_counter < 1:
            return
        rospy.loginfo("Last nearest value removed: {}".format(self.raw_distances_lower.pop()))
        self.distances_counter -= 1
        self.notify_new_distane()

    def notify_new_distane(self):
        if len(self.reference_distances) <= self.distances_counter:
            rospy.loginfo("All distances present. Save, revert or back.")
            return

        rospy.loginfo("Ready to save distance {}m".format(self.reference_distances[self.distances_counter]))

    def clear_all(self):
        self.raw_distances_lower = []
        self.actual_distances = []
        self.distances_counter = 0
        rospy.loginfo("All values cleared")
        self.notify_new_distane()


if __name__ == '__main__':
    try:
        rospy.init_node("ximea_distance_calibration")
        rate = rospy.Rate(60)
        rate.sleep()
        calibration = XimeaDistanceCalib()

        while not rospy.is_shutdown():
            rate.sleep()

    except rospy.ROSInterruptException:
        pass
