#! /usr/bin/env python
import rospy
import diploaf_robot.config_client as config_client
from locating_module.msg import ObjectPositions, ObjectPos, RobotPos
from math import *


class RobotPosition(config_client.ConfigClient):
    def __init__(self):
        config_client.ConfigClient.__init__(self)
        rospy.init_node("robot_position", anonymous=True)
        self.init_config_client()
        self.ximea_object_positions = ObjectPositions()
        self.front_cam_object_positions = ObjectPositions()
        self.robot_pos_publisher = rospy.Publisher("logic_module/robot_pos", RobotPos, queue_size=1)
        rospy.Timer(rospy.Duration(1/60.0), self.calculate_robot_pos)


    def smart_positions_ximea_callback(self, object_positions):
        self.ximea_object_positions = object_positions

    def smart_positions_front_cam_callback(self, object_positions):
        self.front_cam_object_positions = object_positions

    def calculate_robot_pos(self, args):
        robot_pos = RobotPos()
        robot_pos.distanceFromCenter = self.calculate_distance_from_center()
        self.robot_pos_publisher.publish(robot_pos)

    def calculate_distance_from_center(self):
        opponent_gate = self.front_cam_object_positions.OpponentGatePos
        own_gate = self.ximea_object_positions.OwnGatePos

        opponent_gate_dis = opponent_gate.Distance
        own_gate_dis = own_gate.Distance


        if opponent_gate_dis == -1 or opponent_gate_dis == 0 or own_gate_dis == -1 or own_gate_dis == -1:
            return -1

        #
        # rospy.loginfo(opponent_gate_dis)
        # rospy.loginfo(own_gate_dis)
        # rospy.loginfo("___")

        angle = abs(opponent_gate.Angle180 - own_gate.Angle180) % 180

        area = opponent_gate_dis * own_gate_dis * sin(radians(angle)) / 2
        c = sqrt(opponent_gate_dis ** 2 + own_gate_dis ** 2 - own_gate_dis * opponent_gate_dis * cos(radians(angle)))
        h = 2 * area / c
        return h

    def run(self):
        rospy.Subscriber("logic_module/smart_object_positions_ximea", ObjectPositions, self.smart_positions_ximea_callback)
        rospy.Subscriber("logic_module/smart_object_positions_front_cam", ObjectPositions, self.smart_positions_front_cam_callback)
        rospy.Rate(60)
        rospy.spin()


if __name__ == '__main__':
    try:
        RobotPosition().run()
    except rospy.ROSInterruptException:
        pass
