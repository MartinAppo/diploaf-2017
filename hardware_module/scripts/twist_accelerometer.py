#! /usr/bin/env python
import rospy
from geometry_msgs.msg import Twist, Vector3
from time import time
import numpy as np
import diploaf_utils.math_util as math_util


class TwistAccelerometer:
    def __init__(self):
        rospy.init_node("twist_accelerometer")

        self.cmd_vel_pub = rospy.Publisher("hardware_module/accelerometer/cmd_vel", Twist, queue_size=1)
        self.linear_acceleration = rospy.get_param("/linear_acceleration")
        self.angular_acceleration = rospy.get_param("/angular_acceleration")
        self.max_linear_speed = rospy.get_param("/max_linear_speed")
        self.max_angular_speed = rospy.get_param("/max_angular_speed")

        self.current_acc_twist = Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))
        self.input_twist = None
        self.input_last_updated = None

        rospy.Subscriber("hardware_module/cmd_vel", Twist, self.cmd_vel_callback)

    def cmd_vel_callback(self, cmd_vel):
        self.input_twist = cmd_vel
        self.input_twist.linear.x = self.limit_speed(self.input_twist.linear.x, self.max_linear_speed)
        self.input_twist.linear.y = self.limit_speed(self.input_twist.linear.y, self.max_linear_speed)
        self.input_twist.angular.x = self.limit_speed(self.input_twist.angular.x, self.max_angular_speed)
        self.input_last_updated = time()

    def limit_speed(self, input_speed, limit):
        return np.copysign(min(limit, abs(input_speed)), input_speed)

    def run(self):
        rate = rospy.Rate(60)

        while not rospy.is_shutdown():
            # if no input has come then skip
            if self.input_last_updated is None:
                rate.sleep()
                continue

            # if not input has come for half a second and needed speed is reached, then stop the calculations
            if time() - self.input_last_updated > 0.5 \
                    and math_util.get_distance_between_two_vectors(self.current_acc_twist.linear,
                                                                   self.input_twist.linear) <= 0.1 \
                    and self.speeds_equals(self.current_acc_twist.angular.x, self.input_twist.angular.x, self.angular_acceleration):
                rate.sleep()
                continue

            x_in = self.input_twist.linear.x
            y_in = self.input_twist.linear.y
            a_in = self.input_twist.angular.x

            x_cur = self.current_acc_twist.linear.x
            y_cur = self.current_acc_twist.linear.y
            a_cur = self.current_acc_twist.angular.x

            self.current_acc_twist.linear.x = self.accelerate_axis(x_cur, x_in, self.linear_acceleration)
            self.current_acc_twist.linear.y = self.accelerate_axis(y_cur, y_in, self.linear_acceleration)
            #self.current_acc_twist.angular.x = self.accelerate_axis(a_cur, a_in, self.angular_acceleration)
            self.current_acc_twist.angular.x = a_in #acceleration on angular axis removed due to direction locker

            self.cmd_vel_pub.publish(self.current_acc_twist)

            rate.sleep()

    def accelerate_axis(self, current, input_speed, acceleration):
        if self.speeds_equals(current, input_speed, acceleration):
            return input_speed

        diff = math_util.get_distance_between_two_vectors(Vector3(current, 0, 0), Vector3(input_speed, 0, 0))
        acceleration = min((diff / 10), acceleration)

        if current > input_speed:
            accelerated = current - acceleration
        else:
            accelerated = current + acceleration

        return accelerated

    def speeds_equals(self, s1, s2, acceleration):
        return abs(s1 - s2) <= acceleration * 2


if __name__ == '__main__':
    try:
        accelerometer = TwistAccelerometer()
        accelerometer.run()
    except rospy.ROSInterruptException:
        pass
