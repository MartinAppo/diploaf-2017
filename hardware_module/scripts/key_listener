#! /usr/bin/env python
import rospy
from diploaf_utils.msg import KeyEvent
from hardware_module.msg import WheelSpeeds
from locating_module.msg import ObjectPositions
from geometry_msgs.msg import Vector3, Twist
import diploaf_robot.config_client as config_client


class KeyBoardListener(config_client.ConfigClient):
    def __init__(self):
        config_client.ConfigClient.__init__(self)
        rospy.init_node("key_listener", anonymous=True)
        self.init_config_client()
        self.linear_vel_publisher = rospy.Publisher("logic_module/linear_vel", Vector3, queue_size=1)
        self.angular_vel_publisher = rospy.Publisher("logic_module/angular_vel", Vector3, queue_size=1)
        self.wheel_speed_publisher = rospy.Publisher(rospy.get_param("/wheel_speeds_topic"), WheelSpeeds, queue_size=1)
        self.threshold = rospy.get_param("/joy_threshold")
        self.wheel_positions = rospy.get_param("/wheel_positions")
        self.wheel_directions = rospy.get_param("/wheel_directions")
        self.output_twist = Twist()
        self.possible_wheel_numbers = ["1", "2", "3", "4"]

    def test_wheel(self, wheel_nr):
        output_wheel_speeds = WheelSpeeds()
        output_wheel_speeds.wheelSpeeds = [0, 0, 0, 0]
        output_wheel_speeds.wheelSpeeds[self.wheel_positions[wheel_nr]] = 300 * self.wheel_directions[wheel_nr]
        self.wheel_speed_publisher.publish(output_wheel_speeds)

    def key_callback(self, key_event_msg):
        if key_event_msg.pressed and key_event_msg.char == "7":
            if self.config["keyboard_enabled"]:
                self.set_config("keyboard_enabled", 0)
            else:
                self.set_config("keyboard_enabled", 1)

        if key_event_msg.pressed and self.config['keyboard_enabled'] and self.config['manual_control']:
            if key_event_msg.char in self.possible_wheel_numbers:
                wheel_nr = int(key_event_msg.char) - 1
                self.test_wheel(wheel_nr)
                return

            if key_event_msg.char == "w":
                self.output_twist.linear.y = 2
            if key_event_msg.char == "s":
                self.output_twist.linear.y = -2
            if key_event_msg.char == "a":
                self.output_twist.linear.x = -2
            if key_event_msg.char == "d":
                self.output_twist.linear.x = 2
            if key_event_msg.char == "q":
                self.output_twist.angular.x = -5
            if key_event_msg.char == "e":
                self.output_twist.angular.x = 5
            if key_event_msg.char in "wsad":
                self.publish_linear_vel()
            if key_event_msg.char in "qe":
                self.publish_angular_vel()

            if key_event_msg.char == "g":
                if self.config["game_start"]:
                    self.set_config("game_start", 0)
                else:
                    self.set_config("game_start", 1)
            if key_event_msg.char == "x":
                if self.config["ximea_enabled"]:
                    self.set_config("ximea_enabled", 0)
                    self.set_config("front_cam_enabled", 1)
                else:
                    self.set_config("ximea_enabled", 1)
                    self.set_config("front_cam_enabled", 0)
            if key_event_msg.char == "v":
                if self.config["debug_mode"]:
                    self.set_config("debug_mode", 0)
                else:
                    self.set_config("debug_mode", 1)
            if key_event_msg.char == "l":
                if self.config["launcher_enabled"]:
                    self.set_config("launcher_enabled", 0)
                else:
                    self.set_config("launcher_enabled", 1)
            if key_event_msg.char == "y":
                if self.config["lock_direction"]:
                    self.set_config("lock_direction", 0)
                else:
                    self.set_config("lock_direction", 1)
        else:
            if key_event_msg.char == "w":
                self.output_twist.linear.y = 0
            if key_event_msg.char == "s":
                self.output_twist.linear.y = 0
            if key_event_msg.char == "a":
                self.output_twist.linear.x = 0
            if key_event_msg.char == "d":
                self.output_twist.linear.x = 0
            if key_event_msg.char == "q":
                self.output_twist.angular.x = 0
            if key_event_msg.char == "e":
                self.output_twist.angular.x = 0
            if key_event_msg.char in "wsad":
                self.publish_linear_vel()
            if key_event_msg.char in "qe":
                self.publish_angular_vel()

    def publish_linear_vel(self):
        if not self.config['game_start'] and self.config['manual_control']:
        #if self.config['manual_control']:
            self.linear_vel_publisher.publish(self.output_twist.linear)

    def publish_angular_vel(self):
        if not self.config['game_start'] and self.config['manual_control']:
        #if self.config['manual_control']:
            self.angular_vel_publisher.publish(self.output_twist.angular)

    def run(self):
        rospy.Subscriber("diploaf_utils/key_event", KeyEvent, self.key_callback)
        rate = rospy.Rate(60)
        while not rospy.is_shutdown():
            rate.sleep()


if __name__ == '__main__':
    try:
        listener = KeyBoardListener()
        listener.run()
    except rospy.ROSInterruptException:
        pass
