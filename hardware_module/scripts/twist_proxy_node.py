#! /usr/bin/env python
import rospy
from geometry_msgs.msg import Twist
from geometry_msgs.msg import Vector3
from time import time


class TwistProxy():
    def __init__(self):
        self.current_twist_msg = Twist(Vector3(0, 0, 0), Vector3(0, 0, 0))
        self.cmd_vel_publisher = rospy.Publisher("hardware_module/cmd_vel", Twist, queue_size=1)
        rospy.Subscriber("logic_module/angular_vel", Vector3, self.angular_vel_callback)
        rospy.Subscriber("logic_module/linear_vel", Vector3, self.linear_vel_callback)
        rospy.Subscriber("logic_module/cmd_vel", Twist, self.full_twist_callback)
        self.last_updated = time()

    def angular_vel_callback(self, angular_vec):
        self.current_twist_msg.angular = angular_vec
        self.last_updated = time()

    def linear_vel_callback(self, linear_vec):
        self.current_twist_msg.linear = linear_vec
        self.last_updated = time()

    def full_twist_callback(self, twist_msg):
        self.current_twist_msg = twist_msg
        self.last_updated = time()

    def spin_once(self):
        if time() - self.last_updated < 0.5:
            self.cmd_vel_publisher.publish(self.current_twist_msg)


if __name__ == '__main__':
    try:
        rospy.init_node("twist_proxy_node")
        twist_proxy = TwistProxy()

        rate = rospy.Rate(60)

        while not rospy.is_shutdown():
            twist_proxy.spin_once()
            rate.sleep()


    except rospy.ROSInterruptException:
        pass
