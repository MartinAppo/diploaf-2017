#! /usr/bin/env python

import rospy
from hardware_module.mainboard_processor import MainboardProcessor

if __name__ == '__main__':
    try:
        MainboardProcessor()
        rospy.Rate(60)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
