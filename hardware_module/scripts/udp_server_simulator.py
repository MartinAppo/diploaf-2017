#! /usr/bin/env python
import rospy
import socket

UDP_IP = "127.0.0.1"
UDP_PORT = 5005

if __name__ == '__main__':
    try:
        #This node is just for testing purposes
        rospy.init_node("udp_server_simulator", anonymous=True)

        rate = rospy.Rate(60)
        sock = socket.socket(socket.AF_INET,
                             socket.SOCK_DGRAM)
        sock.bind((UDP_IP, UDP_PORT))

        while not rospy.is_shutdown():
            data, addr = sock.recvfrom(1024)
            rospy.loginfo("Received message: {}".format(data))
            rate.sleep()
    except rospy.ROSInterruptException:
        pass
