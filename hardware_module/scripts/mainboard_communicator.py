#! /usr/bin/env python
import rospy
import hardware_module.udp_client as udp_client
from hardware_module.mainboard_processor import COMMAND_REF
from hardware_module.referee_commands_helper import RefereeCommandsHelper
from hardware_module.msg import Launcher, WheelSpeeds
from diploaf_robot.msg import KeyValue

COMMAND_SEPARATOR = ":"


class MainboardCommunicator(udp_client.UdpClient):
    def __init__(self):
        rospy.init_node("mainboard_communicator", anonymous=True)
        udp_client.UdpClient.__init__(self, host=rospy.get_param("/mainboard_ip"),
                                      port=rospy.get_param("/mainboard_port"))
        self.launcher_msg = Launcher()
        self.wheel_speeds = [0, 0, 0, 0]
        self.wheel_speeds_topic = rospy.get_param("/wheel_speeds_topic")
        self.launcher_direction = rospy.get_param("/launcher_direction")
        self.servo_max_val = rospy.get_param("/servo_max_val")
        self.servo_min_val = rospy.get_param("/servo_min_val")
        self.refe_comm_helper = RefereeCommandsHelper()
        self.gui_publisher = rospy.Publisher("rqt_diploaf_dashboard/status", KeyValue, queue_size=10)
        self.mainboard_publisher = rospy.Publisher("mainboard/raw_message", KeyValue, queue_size=10)
        self.last_msg_received = None
        rospy.Timer(rospy.Duration(1), self.monitoring_pub_callback)

    def monitoring_pub_callback(self, arg):
        msg = KeyValue()
        msg.MsgType = "mainboard"
        msg.MsgKey = "mainboard_connecter"
        msg.MsgValue = str(self.last_msg_received)
        self.gui_publisher.publish(msg)

    def message_received(self, recv_msg):
        self.last_msg_received = recv_msg

        msg_type, value = recv_msg.split(COMMAND_SEPARATOR, 1)
        msg_type = msg_type[1:]
        value = value[:-1]

        if msg_type == COMMAND_REF:
            rospy.loginfo("Command module raw message: {}".format(value))
            command = self.refe_comm_helper.receive_command(value)
            if len(command) > 0:
                self.send_ack()
                self.publish_command(command, msg_type)
        else:
            self.publish_command(value, msg_type)

    def publish_command(self, message, msg_type):
        # if msg_type == "speeds":
        #     return
        msg = KeyValue()
        msg.MsgType = msg_type
        msg.MsgValue = message
        self.mainboard_publisher.publish(msg)

    def launcher_callback(self, launcher_msg):
        self.launcher_msg = launcher_msg

    def wheel_speeds_callback(self, wheel_speeds_msg):
        self.wheel_speeds = wheel_speeds_msg.wheelSpeeds

    def run(self):
        rospy.Subscriber("hardware_module/launcher", Launcher, self.launcher_callback)
        rospy.Subscriber(self.wheel_speeds_topic, WheelSpeeds, self.wheel_speeds_callback)

        rate = rospy.Rate(60)
        while not rospy.is_shutdown():
            if self.connected:
                self.send_servo()
                self.send_speeds()
            elif self.connecting:
                rospy.logwarn_throttle(2, "MainboardCommunicator connecting...")
            else:
                rospy.logwarn_throttle(2, "MainboardCommunicator connection failed")

            rate.sleep()

        self.close()

    def send_speeds(self):
        message = "speeds:{m1}:{m2}:{m3}:{m4}:{launcher}".format(m1=round(self.wheel_speeds[0]),
                                                                 m2=round(self.wheel_speeds[1]),
                                                                 m3=round(self.wheel_speeds[2]),
                                                                 m4=round(self.wheel_speeds[3]),
                                                                 launcher=round(self.launcher_msg.motorSpeed * self.launcher_direction))
        self.send_message(message)

    def send_servo(self):
        servo_val = min([self.servo_max_val, self.launcher_msg.servoPos])
        servo_val = max([self.servo_min_val, servo_val])
        self.send_message("servos:{servo}:{servo}".format(servo=servo_val))

    def send_ack(self):
        self.send_message("rf:{}".format(self.refe_comm_helper.get_command_to_send("ACK")))


if __name__ == '__main__':
    try:
        mainboard_communicator = MainboardCommunicator()
        mainboard_communicator.run()
    except rospy.ROSInterruptException:
        pass
