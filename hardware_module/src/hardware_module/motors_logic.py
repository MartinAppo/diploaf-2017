import rospy
from hardware_module.msg import WheelSpeeds
from geometry_msgs.msg import Twist
from std_msgs.msg import Header
from math import *
import diploaf_utils.math_util as math_util
import diploaf_robot.config_client as config_client
import numpy as np


class MotorsLogic(config_client.ConfigClient):
    def __init__(self):
        super(MotorsLogic, self).__init__()
        rospy.init_node("motors_logic", anonymous=True)
        self.init_config_client()
        self.wheel_positions = rospy.get_param("/wheel_positions")
        self.wheel_directions = rospy.get_param("/wheel_directions")
        self.wheel_speeds_topic = rospy.get_param("/wheel_speeds_topic")

        self.DEG_45 = pi / 4
        self.DEG_135 = 0.75 * pi

        self.original_angles = [-self.DEG_45, self.DEG_45, self.DEG_135, -self.DEG_135]
        self.angles = [self.original_angles[i] for i in self.wheel_positions]

        self.wheel_speeds_publisher = rospy.Publisher(self.wheel_speeds_topic, WheelSpeeds, queue_size=1)
        self.max_linear_speed = rospy.get_param("/max_linear_speed")
        self.max_angular_speed = rospy.get_param("/max_angular_speed")
        self.twist_msg = None
        self.wheel_speeds_msg = WheelSpeeds()
        self.wheel_speeds_msg.header = Header()

        self.wheel_distance_from_center = rospy.get_param("/wheel_distance_from_center") #in m
        self.wheel_radius = rospy.get_param("/wheel_radius")  #in m
        self.gear_ratio = rospy.get_param("/gear_ratio")
        self.gear_ratio_inv = 1.0 / self.gear_ratio
        self.wheel_radius_inv = 1.0 / self.wheel_radius
        self.rad_to_rpm_inv = 1.0 / 0.10472

        self.wheel_angles_matrix = np.matrix([[sin(self.angles[0]), cos(self.angles[0]), self.wheel_distance_from_center],
                                              [sin(self.angles[1]), cos(self.angles[1]), self.wheel_distance_from_center],
                                              [sin(self.angles[2]), cos(self.angles[2]), self.wheel_distance_from_center],
                                              [sin(self.angles[3]), cos(self.angles[3]), self.wheel_distance_from_center]])

    def get_wheel_speeds(self):

        direction_and_length = math_util.vector_to_direction_and_length(self.twist_msg.linear)
        limited_speed_vector_len = min(direction_and_length['len'], self.max_linear_speed)
        direction = direction_and_length['dir']

        self.twist_msg.linear = math_util.direction_and_length_to_vector(direction, limited_speed_vector_len)

        angular_speed_sign = np.sign(self.twist_msg.angular.x)
        limited_angular_speed = min(abs(self.twist_msg.angular.x), self.max_angular_speed)
        limited_angular_speed = np.copysign(limited_angular_speed, angular_speed_sign)

        target_matrix = np.matrix([[self.twist_msg.linear.x], [self.twist_msg.linear.y], [limited_angular_speed * -1]])

        result_matrix = self.wheel_angles_matrix * target_matrix
        result_array = np.squeeze(np.asarray(result_matrix))
        # * 60: rpm -> rps
        result_array = [result_array[i] * -1 * self.gear_ratio * self.rad_to_rpm_inv * self.wheel_radius_inv * self.wheel_directions[self.wheel_positions[i]] for i in self.wheel_positions]

        #rospy.loginfo_throttle(1, "wheelspeeds: {}".format(result_array))

        return result_array

    def twist_callback(self, twist_msg):
        self.twist_msg = twist_msg

        self.wheel_speeds_msg.wheelSpeeds = self.get_wheel_speeds()
        self.wheel_speeds_msg.header.stamp = rospy.Time.now()
        self.wheel_speeds_publisher.publish(self.wheel_speeds_msg)

    def run(self):
        rospy.Subscriber("hardware_module/accelerometer/cmd_vel", Twist, self.twist_callback)
        rospy.spin()
