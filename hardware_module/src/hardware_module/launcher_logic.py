import rospy
from hardware_module.msg import Launcher
from locating_module.msg import ObjectPositions
import hardware_module.launcher_calibrator as launcher_calibrator
import diploaf_robot.config_client as config_client
import numpy as np
import json


class LauncherLogic(config_client.ConfigClient):
    def __init__(self):
        super(LauncherLogic, self).__init__()
        rospy.init_node("launcher_logic", anonymous=True)
        self.init_config_client()

        with open(launcher_calibrator.LAUNCHER_CONF_PATH, 'r') as json_file:
            self.conf = json.loads(json_file.read())

        self.launch_publisher = rospy.Publisher("hardware_module/launcher", Launcher, queue_size=1)
        self.launcher_msg = Launcher()
        self.object_positions = ObjectPositions()
        self.default_servo_pos = 1000
        self.launcher_direction = rospy.get_param("/launcher_direction")
        self.launcher_motor_speed_offset = rospy.get_param('/launcher_motor_speed_offset')
        self.opponent_gate_distance_med = 0
        self.callback_counter = 0
        self.distances_med = []
        self.launcher_stop_flag = False
        self.servo_prev_val = self.default_servo_pos

    @staticmethod
    def lin_interpolate_launch(d, launch_distances, launch_speeds):
        last_index = len(launch_distances) - 1

        for idx, dist in enumerate(launch_distances):
            if dist == d:
                return launch_speeds[idx]

            if idx < last_index and dist <= d <= launch_distances[idx + 1]:
                launch_dist_diff = launch_distances[idx + 1] - dist
                d_diff = d - dist
                percent = d_diff / float(launch_dist_diff)
                dist_diff = launch_speeds[idx + 1] - launch_speeds[idx]
                return dist_diff * percent + launch_speeds[idx]

            if d < dist and idx == 0:
                return launch_speeds[idx]

            if d > dist and idx == last_index:
                return launch_speeds[-1]

    def get_launcher_conf(self, distance):
        conf = self.conf["servo_values"].itervalues().next()
        servo_val = self.default_servo_pos

        for idx, servo_range in enumerate(self.conf["ranges"]):
            servo_val_key = servo_range[0]
            min_distance = servo_range[1]
            max_distance = servo_range[2]
            if min_distance <= distance <= max_distance:
                conf = self.conf["servo_values"][str(servo_val_key)]
                servo_val = servo_val_key
                # preference already set servo value to avoid jittering between values
                if self.default_servo_pos == servo_val:
                    break
                self.default_servo_pos = servo_val

        if abs(self.servo_prev_val - servo_val) >= 200:
            if self.servo_prev_val > servo_val:
                servo_val = self.servo_prev_val - 200
            else:
                servo_val = self.servo_prev_val + 200
        self.servo_prev_val = servo_val
        return conf[0], conf[1], servo_val

    def object_positions_callback(self, object_positions_msg):
        self.object_positions = object_positions_msg
        if self.callback_counter >= 5:
            self.opponent_gate_distance_med = np.median(self.distances_med)
            self.callback_counter = 0
            self.distances_med = []
        else:
            self.distances_med.append(self.object_positions.OpponentGatePos.Distance)
            self.callback_counter += 1

    def run(self):
        rospy.Subscriber("locating_module/realsense/object_positions", ObjectPositions, self.object_positions_callback)

        rate = rospy.Rate(60)

        while not rospy.is_shutdown():
            if self.config['launcher_enabled']:
                self.launcher_stop_flag = False

                if self.config['launcher_fixed_speed'] and False: #TODO taasta
                    launcher_msg = Launcher()
                    launcher_msg.motorSpeed = rospy.get_param('/launcher_fixed_speed')
                    launcher_msg.servoPos = self.default_servo_pos
                else:
                    gate_distance = self.opponent_gate_distance_med
                    launch_distances, launch_speeds, servo_val = self.get_launcher_conf(gate_distance)

                    new_launch_speeds = []

                    for speed in launch_speeds:
                        speed = speed + self.launcher_motor_speed_offset
                        new_launch_speeds.append(speed)

                    launch_speeds = new_launch_speeds

                    launcher_msg = Launcher()
                    launcher_msg.motorSpeed = self.lin_interpolate_launch(gate_distance, launch_distances, launch_speeds)
                    launcher_msg.servoPos = servo_val

                self.launch_publisher.publish(launcher_msg)
            else:
                if not self.launcher_stop_flag:
                    self.launch_publisher.publish(Launcher())
                self.launcher_stop_flag = True
            rate.sleep()
