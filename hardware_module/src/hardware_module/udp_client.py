import socket
import asyncore
import threading
from collections import deque


class UdpClient(asyncore.dispatcher):
    def __init__(self, host="192.168.4.1", port=8042):
        asyncore.dispatcher.__init__(self)
        self.create_socket(socket.AF_INET,
                           socket.SOCK_DGRAM)
        self.connect((host, port))
        self.buffer_size = 10
        self.buffer = deque(maxlen=self.buffer_size)
        self.thread = threading.Thread(target=asyncore.loop, kwargs={'timeout': 0.015}, name="Asyncore Loop")
        self.thread.start()

    def send_message(self, message):
        self.buffer.append(message)

    def message_received(self, recv_msg):
        return

    def handle_connect(self):
        pass

    def handle_close(self):
        self.close()

    def handle_read(self):
        recv_msg = self.recv(64)
        self.message_received(recv_msg)

    def writable(self):
        return len(self.buffer) > 0

    def handle_write(self):
        self.send_single_message(self.buffer.pop())

    def send_single_message(self, message):
        while len(message) > 0:
            sent = self.send(message)
            message = message[sent:]
