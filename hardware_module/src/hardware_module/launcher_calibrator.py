from hardware_module.msg import Launcher
from locating_module.msg import ObjectPositions
from locating_module.msg import ObjectPos
from diploaf_utils.key_listener import KeyListener
from locating_module.srv import DriveTo, DriveToRequest, DriveToResponse
from geometry_msgs.msg import Vector3
import diploaf_utils.math_util as math_util
from diploaf_robot.msg import KeyValue
import diploaf_robot.config_client as config_client
import numpy as np
import rospy
import rospkg
import json

PACKAGE_PATH = rospkg.RosPack().get_path('hardware_module')
LAUNCHER_CONF_PATH = '{}/conf/launcher_conf_{}.json'.format(PACKAGE_PATH, rospy.get_param("/robot_name"))


class LauncherCalibrator(config_client.ConfigClient, KeyListener):
    def __init__(self):
        # init
        config_client.ConfigClient.__init__(self)
        KeyListener.__init__(self)
        self.init_config_client()
        self.servo_values = []
        self.servo_step = 10

        self.motor_values = []
        self.motor_step = 10

        self.distances = []

        self.current_servo = 1000
        self.current_motor = 800

        self.launcher_msg = Launcher()
        self.target_pos = ObjectPos()
        self.target_distance_med = 0
        self.dist_med_list = []
        self.counter = 0
        self.drive_back_counter = 0

        self.publisher = None
        self.obj_sub = None
        self.launcher_sub = None
        self.gui_publisher = None
        self.current_message = ""

        self.init()

    def init(self):
        rospy.loginfo("== Launcher calibrator ==")
        rospy.loginfo("Arrow Up    - to add motor speed")
        rospy.loginfo("Arrow Down  - to reduce motor speed")
        rospy.loginfo("Arrow Right - to rise servo pwm")
        rospy.loginfo("Arrow Left  - to reduce servo pwm")
        rospy.loginfo("     b      - to clear last value")
        rospy.loginfo("     r      - to reset all values")
        rospy.loginfo("     c      - to save current value")
        rospy.loginfo("     s      - to save all values")
        rospy.loginfo("     n      - to to reverse the robot 0.25m")
        rospy.loginfo("     m      - to drive forward 0.25m")
        rospy.loginfo("=========================")

        self.publisher = rospy.Publisher("hardware_module/launcher", Launcher, queue_size=1)
        self.obj_sub = rospy.Subscriber("locating_module/realsense/object_positions", ObjectPositions,
                                        self.object_positions_callback)
        self.launcher_sub = rospy.Subscriber("hardware_module/launcher", Launcher,
                                             self.launcher_callback)
        self.gui_publisher = rospy.Publisher("rqt_diploaf_dashboard/status", KeyValue, queue_size=1)
        self.call_drive_to = rospy.ServiceProxy("robot_driver_node/drive_to", DriveTo)

        self.update_values()
        rospy.Timer(rospy.Duration(1), self.monitoring_pub_callback)

    def monitoring_pub_callback(self, arg):
        self.publish_gui_status()

    def publish_gui_status(self):
        msg = KeyValue()
        msg.MsgType = "launcher_calib"
        msg.MsgKey = "launcher_calib"
        msg.MsgValue = self.current_message
        self.gui_publisher.publish(msg)

    def drive(self, meters_to_drive):
        rospy.wait_for_service("robot_driver_node/drive_to")
        rospy.loginfo("Driving {}m".format(meters_to_drive))
        vect = math_util.direction_and_length_to_vector(0, meters_to_drive)
        self.call_drive_to(DriveToRequest(Vector3(vect.x, vect.y, 0), None))

    def key_event_callback(self, key_event_msg):
        if self.config['manual_control']:
            return #If manual control is activated, we dont want to use calibration buttons

        if key_event_msg.pressed:
            return

        if key_event_msg.char == "Up":
            self.current_motor = self.current_motor + self.motor_step
            rospy.loginfo("Motor:{}".format(self.current_motor))

        if key_event_msg.char == "Down":
            self.current_motor = self.current_motor - self.motor_step
            rospy.loginfo("Motor:{}".format(self.current_motor))

        if key_event_msg.char == "Left":
            self.current_servo = self.current_servo - self.servo_step
            rospy.loginfo("Servo:{}".format(self.current_servo))

        if key_event_msg.char == "Right":
            self.current_servo = self.current_servo + self.servo_step
            rospy.loginfo("Servo:{}".format(self.current_servo))

        if key_event_msg.char == "b":  # back
            self.clear_last()
            return

        if key_event_msg.char == "r":  # reset
            self.clear_all()
            return

        if key_event_msg.char == "c":  # current
            self.save_current()
            return

        if key_event_msg.char == "s":  # save
            self.save_all()
            return

        if key_event_msg.char == "n":  # reverese
            self.drive(-0.25)
            return

        if key_event_msg.char == "m":  # forward
            self.drive(0.25)
            return

        self.update_values()

    def update_values(self):
        if self.publisher is not None:
            self.launcher_msg.servoPos = self.current_servo
            self.launcher_msg.motorSpeed = self.current_motor
            self.publisher.publish(self.launcher_msg)

    def object_positions_callback(self, object_pos_msg):
        self.target_pos = object_pos_msg.OpponentGatePos
        if self.counter >= 30:
            self.target_distance_med = np.median(self.dist_med_list)
            self.counter = 0
            self.dist_med_list = []
        else:
            self.dist_med_list.append(self.target_pos.Distance)
            self.counter += 1

    def launcher_callback(self, launcher_msg):
        self.launcher_msg = launcher_msg
        self.current_servo = launcher_msg.servoPos
        self.current_motor = launcher_msg.motorSpeed

    def spin_once(self):
        pass

    def save_current(self):
        distance = self.target_distance_med
        self.servo_values.append(self.current_servo)
        self.motor_values.append(self.current_motor)
        self.distances.append(distance)
        self.current_message = "Record saved: servo:{}, motor:{}, distance:{}".format(self.current_servo,
                                                                                      self.current_motor, distance)
        rospy.loginfo(self.current_message)

    def clear_all(self):
        self.motor_values = []
        self.servo_values = []
        self.distances = []
        self.current_message = "All conf cleared"
        rospy.loginfo(self.current_message)

    def clear_last(self):
        if len(self.motor_values) < 1:
            return

        self.motor_values.pop()
        self.servo_values.pop()
        self.distances.pop()
        self.current_message = "Last record cleared"
        rospy.loginfo(self.current_message)

    def save_all(self):
        conf = {"ranges": [], "servo_values": {}}

        for idx, servo_val in enumerate(self.servo_values):
            curr_servo_val = self.servo_values[idx]

            if curr_servo_val in conf["servo_values"]:
                conf["servo_values"][curr_servo_val][0].append(self.distances[idx])
                conf["servo_values"][curr_servo_val][1].append(self.motor_values[idx])
            else:
                conf["servo_values"][curr_servo_val] = [[self.distances[idx]], [self.motor_values[idx]]]

        for servo_val, values in conf["servo_values"].iteritems():
            distances = values[0]
            conf["ranges"].append([servo_val, min(distances), max(distances)])

        with open(LAUNCHER_CONF_PATH, 'w') as json_file:
            json_file.write(json.dumps(conf))

        self.current_message = "All values saved"
        rospy.loginfo(self.current_message)
        rospy.loginfo("conf: {}".format(conf))
