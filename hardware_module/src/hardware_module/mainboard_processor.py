import rospy
import diploaf_robot.config_client as config_client
from diploaf_robot.msg import KeyValue
from hardware_module.srv import *
from hardware_module.msg import WheelSpeeds
import std_msgs.msg

COMMAND_BALL = "ball"
COMMAND_SPEEDS = "speeds"
COMMAND_REF = "ref"


class MainboardProcessor(config_client.ConfigClient):
    def __init__(self):
        config_client.ConfigClient.__init__(self)
        rospy.init_node("mainboard_processor", anonymous=True)
        self.init_config_client()
        rospy.Subscriber("mainboard/raw_message", KeyValue, self.message_callback)
        self.motor_speed_publisher = rospy.Publisher("hardware_module/motor_speed_feedback", WheelSpeeds, queue_size=1)

        self.is_ball_shot = False
        self.balls_shot_count = 0
        self.is_ball_in = False

        self.motor_speeds = [0, 0, 0, 0, 0]
        self.motor_speeds_feedback_msg = WheelSpeeds()
        self.motor_speeds_feedback_msg.header = std_msgs.msg.Header()


        self.init_services()

    def init_services(self):
        rospy.Service("mainboard_processor/get_ball_params", BallParameters, self.handle_get_ball_params)
        rospy.Service("mainboard_processor/get_speeds", MotorSpeeds, self.handle_get_speeds)

    def handle_get_ball_params(self, req):
        ball_parameters_resp = self.is_ball_shot, self.is_ball_in, self.balls_shot_count
        self.is_ball_shot = False
        return ball_parameters_resp

    def handle_get_speeds(self, req):
        return self.motor_speeds

    def message_callback(self, message):
        msg_type = message.MsgType
        msg = message.MsgValue

        if msg_type == COMMAND_BALL:
            self.process_ball_msg(msg)
            return

        if msg_type == COMMAND_SPEEDS:
            self.process_speeds_msg(msg)
            return

        if msg_type == COMMAND_REF:
            self.process_referee_msg(msg)
            return

    def process_ball_msg(self, msg):
        new_ball_in = int(msg)

        if self.is_ball_in == 1 and new_ball_in == 0 and not self.is_ball_shot:
            self.balls_shot_count += 1
            self.is_ball_shot = True

        self.is_ball_in = new_ball_in

    def process_speeds_msg(self, msg):
        speeds = msg.split(":")
        self.motor_speeds = float(speeds[0]), float(speeds[1]), float(speeds[2]), float(speeds[3]), float(speeds[4])
        self.motor_speeds_feedback_msg.wheelSpeeds = self.motor_speeds
        self.motor_speeds_feedback_msg.header.stamp = rospy.Time.now()
        self.motor_speed_publisher.publish(self.motor_speeds_feedback_msg)

    def process_referee_msg(self, msg):
        if "START" in msg:
            self.set_config("game_start", 1)

        if "STOP" in msg:
            self.set_config("game_start", 0)
