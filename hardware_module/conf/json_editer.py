import sys
import os
import json

if sys.version_info[0] < 3:
    import Tkinter as Tk
else:
    import tkinter as Tk


__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))

master = Tk.Tk()

robot_nr = sys.argv[1] if len(sys.argv) > 1 else "1"

file = open(os.path.join(__location__, "launcher_conf_robot" + robot_nr + ".json"), "r+")
print(robot_nr)
data = json.loads(file.read())
servo_values = data["servo_values"]

m_row = 1
speeds = []
distances = []


class Entry:
    tkEntry = {}
    value = 0
    servo = 0
    indx = 0

    def __init__(self, e, value, s, ix):
        self.tkEntry = e
        self.value = value
        self.servo = s
        self.indx = ix


for key in servo_values:
    Tk.Label(master, text="servo pos: " + key).grid(row=m_row)
    m_row += 1
    for i in range(len(servo_values[key][0])):
        Tk.Label(master, text="distance: " + str(servo_values[key][0][i])).grid(row=m_row)
        entry = Tk.Entry(master)
        entry.grid(row=m_row, column=1)
        distances.append(Entry(entry, servo_values[key][0][i], key, i))
        Tk.Label(master, text="speed: " + str(servo_values[key][1][i])).grid(row=m_row, column=2)
        entry = Tk.Entry(master)
        entry.grid(row=m_row, column=3)
        speeds.append(Entry(entry, servo_values[key][0][i], key, i))
        m_row += 1


def save():
    for i in range(len(distances)):
        if distances[i].tkEntry.get() != "":
            servo_values[distances[i].servo][0][distances[i].indx] = float(distances[i].tkEntry.get())
        if speeds[i].tkEntry.get() != "":
            servo_values[speeds[i].servo][1][speeds[i].indx] = float(speeds[i].tkEntry.get())

    file.seek(0)
    file.write(json.dumps(data))
    file.truncate()
    quit()


def quit():
    file.close()
    master.quit()


Tk.Button(master, text='Quit', command=quit).grid(row=m_row, column=0, sticky=Tk.W, pady=4)
Tk.Button(master, text='Save', command=save).grid(row=m_row, column=1, sticky=Tk.W, pady=4)

Tk.mainloop()
