import matplotlib
matplotlib.use('TkAgg')
import numpy as np
import json
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2TkAgg
from matplotlib.backend_bases import key_press_handler
from matplotlib.figure import Figure
import sys
if sys.version_info[0] < 3:
    import Tkinter as Tk
else:
    import tkinter as Tk

import os

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))

root = Tk.Tk()
root.wm_title("Launcher distances")

f = Figure(figsize=(7, 6), dpi=100)
a = f.add_subplot(111)

robot_nr = sys.argv[1] if len(sys.argv) > 1 else "1"

file = open(os.path.join(__location__, "launcher_conf_robot" + robot_nr + ".json"), "r")
print(robot_nr)
data = json.loads(file.read())
servo_values = data["servo_values"]

legend = []

for key in servo_values:
    legend += ["servo pos: " + key]
    distances = np.array(servo_values[key][0])
    motor_speeds = np.array(servo_values[key][1])
    a.plot(distances, motor_speeds, marker='o', linestyle='dashed')

a.legend(legend, loc='upper left')
a.set_xlabel("Distances")
a.set_ylabel("Motor speeds")

# a tk.DrawingArea
canvas = FigureCanvasTkAgg(f, master=root)
canvas.show()
canvas.get_tk_widget().pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

toolbar = NavigationToolbar2TkAgg(canvas, root)
toolbar.update()
canvas._tkcanvas.pack(side=Tk.TOP, fill=Tk.BOTH, expand=1)

def _quit():
    root.quit()
    root.destroy()

button = Tk.Button(master=root, text='Quit', command=_quit)
button.pack(side=Tk.BOTTOM)

Tk.mainloop()