import sys
import os
import json
import numpy as np

if sys.version_info[0] < 3:
    import Tkinter as Tk
else:
    import tkinter as Tk

__location__ = os.path.realpath(
    os.path.join(os.getcwd(), os.path.dirname(__file__)))

master = Tk.Tk()

robot_nr = sys.argv[1] if len(sys.argv) > 1 else "1"

file = open(os.path.join(__location__, "front_cam_distances_robot" + robot_nr + ".json"), "r+")
print(robot_nr)
data = json.loads(file.read())

distances = np.array(data[1])
pixel_values = np.zeros(distances.shape)
pixel_values[:np.array(data[0]).shape[0]] = np.array(data[0])



pixelEntries = []
distanceEntries = []


class Entry:
    tkEntry = {}
    value = 0

    def __init__(self, e, value):
        self.tkEntry = e
        self.value = value


for i in range(len(distances)):
    Tk.Label(master, text="distance: " + str(distances[i])).grid(row=i)
    entry = Tk.Entry(master)
    entry.grid(row=i, column=1)
    distanceEntries.append(Entry(entry, distances[i]))
    Tk.Label(master, text="px val: " + str(pixel_values[i])).grid(row=i, column=2)
    entry = Tk.Entry(master)
    entry.grid(row=i, column=3)
    pixelEntries.append(Entry(entry, pixel_values[i]))


def save():
    for j in range(len(distanceEntries)):
        if distanceEntries[j].tkEntry.get() != "":
            distances[j] = float(distanceEntries[j].tkEntry.get())
        if pixelEntries[j].tkEntry.get() != "":
            pixel_values[j] = float(pixelEntries[j].tkEntry.get())

    data[1] = distances.tolist()
    data[0] = pixel_values.tolist()
    file.seek(0)
    file.write(json.dumps(data))
    file.truncate()
    quit()


def quit():
    file.close()
    master.quit()


Tk.Button(master, text='Quit', command=quit).grid(row=i+1, column=0, sticky=Tk.W, pady=4)
Tk.Button(master, text='Save', command=save).grid(row=i+1, column=1, sticky=Tk.W, pady=4)

Tk.mainloop()
