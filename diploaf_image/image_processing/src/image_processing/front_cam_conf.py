import rospkg
import subprocess
import rospy


# General camera conf
CAM_ID = rospy.get_param("/front_cam_id")
DEVICE_ID = rospy.get_param("/front_cam_device_id")
FPS = rospy.get_param("/front_cam_fps")
PACKAGE_PATH = rospkg.RosPack().get_path('image_processing')
COLORS_CONF_PATH = '{}/conf/front_cam_colors_{}.pkl'.format(PACKAGE_PATH, rospy.get_param("/robot_name"))
DISTANCES_CONF_PATH = '{}/conf/front_cam_distances_{}.json'.format(PACKAGE_PATH, rospy.get_param("/robot_name"))
WIDTH = rospy.get_param("/front_cam_width")
HEIGHT = rospy.get_param("/front_cam_height")
DEPTH_WIDTH = rospy.get_param("/front_cam_depth_w")
DEPTH_HEIGHT = rospy.get_param("/front_cam_depth_h")

COLORS = {
    "balls": 1
    , "target_blue": 3
    , "target_magenta": 2
    , "field": 4
    , "black": 6
    , "white": 5
}

'''
wide_angle_cam_properties:
                     brightness (int)    : min=-64 max=64 step=1 default=0 value=0
                       contrast (int)    : min=0 max=64 step=1 default=32 value=32
                     saturation (int)    : min=0 max=128 step=1 default=60 value=80
                            hue (int)    : min=-40 max=40 step=1 default=0 value=20
 white_balance_temperature_auto (bool)   : default=1 value=0
                          gamma (int)    : min=72 max=500 step=1 default=100 value=117
                           gain (int)    : min=0 max=100 step=1 default=0 value=0
           power_line_frequency (menu)   : min=0 max=2 default=1 value=1
      white_balance_temperature (int)    : min=2800 max=6500 step=1 default=4600 value=3638
                      sharpness (int)    : min=0 max=6 step=1 default=2 value=2
         backlight_compensation (int)    : min=0 max=2 step=1 default=1 value=0
                  exposure_auto (menu)   : min=0 max=3 default=3 value=1
              exposure_absolute (int)    : min=1 max=5000 step=1 default=157 value=100
         exposure_auto_priority (bool)   : default=0 value=0

'''


@DeprecationWarning
def apply_front_cam_conf():
    # This has to be called two times, because otherwise ps3eye fails to apply conf the first time.
    front_cam_conf()
    front_cam_conf()

@DeprecationWarning
def front_cam_conf():
    for x in range(0, 10):
        try:
            subprocess.check_output('v4l2-ctl -d /dev/video{} -c '.format(x) +
                                    _get_camera_parameters(), shell=True)
            rospy.loginfo('Ps3eye conf: cam settings applied to /dev/video{}'.format(x))
            return
        except subprocess.CalledProcessError:
            rospy.logwarn('Ps3eye conf: /dev/video{} not supported'.format(x))

@DeprecationWarning
def _get_camera_parameters():
    if rospy.get_param("/front_cam") == "wide_angle_cam":
        return 'brightness=0' \
               + ',contrast=24' \
               + ',saturation=64' \
               + ',hue=0' \
               + ',gamma=96' \
               + ',white_balance_temperature_auto=0' \
               + ',gain=5' \
               + ',sharpness=3'
    if rospy.get_param("/front_cam") == "ps3_eye":
        return 'white_balance_automatic=0' \
               + ',gain_automatic=0' \
               + ',auto_exposure=1' \
               + ',brightness=0' \
               + ',contrast=24' \
               + ',saturation=64' \
               + ',hue=0' \
               + ',red_balance=96' \
               + ',blue_balance=255' \
               + ',gamma=96' \
               + ',exposure=96' \
               + ',gain=16' \
               + ',sharpness=5' \
               + ',vertical_flip=1' \
               + ',horizontal_flip=1'
