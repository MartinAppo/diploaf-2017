import rospy
import pyXiQ
import image_processing.ximea_conf as conf


class XimeaCamera():
    def __init__(self):
        self.cam = None
        self.run = False

    def init_cam(self):
        self.cam = pyXiQ.Camera()

        if not self.cam.opened():
            rospy.logerr("XIMEA camera missing")
            self.run = False
            return

        conf.set_cam_conf(self.cam)

        self.additional_cam_init()
        self.cam.start()
        self.run = True

    def additional_cam_init(self):
        pass
