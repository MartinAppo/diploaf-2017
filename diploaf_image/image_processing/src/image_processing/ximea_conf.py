import rospkg
import numpy
import rospy

COLORS = {
    "balls": 1
    , "target_blue": 2
    , "target_magenta": 3
    , "field": 4
    , "black": 5
    , "white": 6
    , "other": 7
}

PACKAGE_PATH = rospkg.RosPack().get_path('image_processing')
LOCATING_PACKAGE_PATH = rospkg.RosPack().get_path('locating_module')

XIMEA_COLORS_CONF_PATH = '{}/conf/ximea_colors_{}.pkl'.format(PACKAGE_PATH, rospy.get_param("/robot_name"))
XIMEA_ACTIVE_PIXELS_PATH = '{}/conf/ximea_active_pixels_{}.pkl'.format(PACKAGE_PATH, rospy.get_param("/robot_name"))
XIMEA_DISTANCES_CONF_PATH = '{}/conf/ximea_distances_{}.json'.format(LOCATING_PACKAGE_PATH, rospy.get_param("/robot_name"))
HEIGHT = 1024
WIDTH = 1280
MID_Y = HEIGHT // 2
MID_X = WIDTH // 2
MID_POS = numpy.array((MID_X, MID_Y))
MID_TOP_POS = numpy.array((MID_X, 0))
MID_RIGHT_POS = numpy.array((WIDTH, MID_Y))
MID_LEFT_POS = numpy.array((0, MID_Y))


# Kui debug mode peal, siis ule 25fps ei lahe
# 10000 == ~ 60 fps
# 20000 == ~ 42-54 fps
# 25000 == ~ 34-42 fps
def set_cam_conf(cam):
    cam.setInt("exposure", rospy.get_param("ximea_exposure"))
    cam.setInt("auto_wb", 0)
    cam.setInt("gain", rospy.get_param("ximea_gain"))
    cam.setFloat("sharpness", 4.0)
    cam.setInt("buffers_queue_size", 1)
    cam.setFloat("wb_kb", 4.0)
    cam.setFloat("wb_kg", 2.0)
    cam.setFloat("wb_kr", 1.0)
