import numpy as np


def get_color_coverage(mask):
    whole_count = np.sum(np.ones(mask.shape))
    pixel_count = np.sum(mask)

    if whole_count < 1:
        return 100

    if pixel_count < 1:
        return 0

    return pixel_count / whole_count * 100
