#!/usr/bin/env python

import cPickle as pickle
import cv2
import numpy as np
import rospy
import pyXiQ
import image_processing.ximea_conf as conf
from image_processing.ximea_camera import XimeaCamera
from sensor_msgs.msg import CompressedImage
from geometry_msgs.msg import Point
from diploaf_utils.key_listener import KeyListener
from diploaf_robot.msg import KeyValue
import diploaf_robot.config_client as config_client
import time


class XimeaColorCalibrator(XimeaCamera, KeyListener, config_client.ConfigClient):
    def __init__(self):
        config_client.ConfigClient.__init__(self)
        KeyListener.__init__(self)
        XimeaCamera.__init__(self)
        self.init_config_client()
        self.init_cam()

        self.color_file = conf.XIMEA_COLORS_CONF_PATH
        self.image = self.cam.image()
        self.mouse_x = 0
        self.mouse_y = 0
        self.brush_size = 1
        self.noise = 1
        self.color = 0
        self.color_name = ""
        self.update_i = 0
        self.i = 0
        self.h = self.w = self.ys = self.xs = self.active_pixels = None
        self.image_pub = rospy.Publisher("image_processing/ximea_img/compressed", CompressedImage, queue_size=1)
        self.image_pub_fragmented = rospy.Publisher("image_processing/ximea_img_fragmented/compressed", CompressedImage,
                                                    queue_size=1)
        self.gui_publisher = rospy.Publisher("rqt_diploaf_dashboard/status", KeyValue, queue_size=1)
        rospy.Subscriber("/image_processing/ximea_img/compressed_mouse_left", Point,
                         self.mouse_click_callback)
        rospy.Subscriber("/image_processing/ximea_img_fragmented/compressed_mouse_left", Point,
                         self.mouse_click_callback)
        self.current_message = ""

        try:
            with open(self.color_file, 'rb') as fh:
                self.colors_lookup = pickle.load(fh)
        except:
            self.colors_lookup = np.zeros(0x1000000, dtype=np.uint8)

        self.init_active_pixels()
        rospy.Timer(rospy.Duration(1), self.monitoring_pub_callback)

    def monitoring_pub_callback(self, arg):
        self.publish_gui_status()

    def publish_gui_status(self):

        msg = KeyValue()
        msg.MsgType = "ximea_calib"
        msg.MsgKey = "ximea_calib"
        msg.MsgValue = self.current_message
        self.gui_publisher.publish(msg)

    def init_active_pixels(self):
        self.h, self.w, _ = self.image.shape
        self.ys, self.xs = np.mgrid[:self.h, :self.w]
        self.active_pixels = np.ones((self.h, self.w), dtype=np.uint8)
        # pixels which are outside the circle r=500 (x=640, y=512)
        circle_radius = rospy.get_param("ximea_circle_radius")
        ximea_ball_circle_radius = rospy.get_param("ximea_ball_circle_radius")

        y_offset = rospy.get_param("ximea_radius_y_offset")
        x_offset = rospy.get_param("ximea_radius_x_offset")

        self.active_pixels[(self.ys - (conf.HEIGHT / 2 + y_offset)) ** 2 + (
                self.xs - (conf.WIDTH / 2 + x_offset)) ** 2 > circle_radius ** 2] = 0

    def nothing(self, x):
        pass

    def change_color(self):
        self.update_i -= 1
        # brush (select adjacent pixels)
        ob = self.image[
             max(0, self.mouse_y - self.brush_size):min(self.image.shape[0], self.mouse_y + self.brush_size + 1),
             max(0, self.mouse_x - self.brush_size):min(self.image.shape[1], self.mouse_x + self.brush_size + 1),
             :].reshape((-1, 3)).astype('int32')
        # add noise (select close colors)
        noises = xrange(-self.noise, self.noise + 1)
        for a in noises:
            for b in noises:
                for c in noises:
                    self.colors_lookup[
                        ((ob[:, 0] + a) + (ob[:, 1] + b) * 0x100 + (ob[:, 2] + c) * 0x10000).clip(0,
                                                                                                  0xffffff)] = self.color

    def mouse_click_callback(self, point):
        self.choose_color(cv2.EVENT_LBUTTONDOWN, int(point.x), int(point.y), None, None)
        rospy.loginfo("Point: {}".format(point))

    def choose_color(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            self.mouse_x = x
            self.mouse_y = y
            self.update_i = 5
            self.change_color()

    def spin(self):
        self.image = self.cam.image()

        if self.update_i > 0:
            self.change_color()

        self.i += 1
        if self.i % 1 == 0:  # display every n-th frame
            self.i = 0
            self.image[self.active_pixels == 0] = [0, 0, 0]
            cv2.putText(self.image, self.color_name, (10, 500), cv2.FONT_HERSHEY_SIMPLEX, 2,
                        (255, 255, 255), 2, cv2.LINE_AA)

            # threshold
            self.image = self.image.astype('uint32')
            fragmented = self.colors_lookup[
                self.image[:, :, 0] + self.image[:, :, 1] * 0x100 + self.image[:, :, 2] * 0x10000]

            # show colors 0-8
            frame = self.image.astype('uint8')
            frame[fragmented == conf.COLORS['balls']] = np.array([0, 0, 255], dtype=np.uint8)
            frame[fragmented == conf.COLORS['target_blue']] = np.array([255, 0, 0], dtype=np.uint8)
            frame[fragmented == conf.COLORS['target_magenta']] = np.array([255, 0, 255], dtype=np.uint8)
            frame[fragmented == conf.COLORS['field']] = np.array([0, 255, 0], dtype=np.uint8)
            frame[fragmented == conf.COLORS['black']] = np.array([255, 255, 0], dtype=np.uint8)
            frame[fragmented == conf.COLORS['white']] = np.array([255, 255, 255], dtype=np.uint8)
            frame[fragmented == conf.COLORS['other']] = np.array([0, 0, 0], dtype=np.uint8)
            self.publish_img(self.image.astype('uint8'), self.image_pub)
            self.publish_img(frame, self.image_pub_fragmented)

    def key_event_callback(self, key_event_msg):

        if key_event_msg.pressed:
            return

        #if manual control is activated, then ignore calibration keys
        if self.config['manual_control']:
            return

        message = ""

        if key_event_msg.char == "Up":
            self.brush_size = self.brush_size + 1

        if key_event_msg.char == "Down":
            self.brush_size = max(self.brush_size - 1, 0)

        if key_event_msg.char == "Right":
            self.noise += 1

        if key_event_msg.char == "Left":
            self.noise -= 1

        self.noise = max(self.noise, 0)

        if key_event_msg.char == "s":
            with open(self.color_file, 'wb') as fh:
                pickle.dump(self.colors_lookup, fh, -1)
            message += "Color calibration saved; "

            with open(conf.XIMEA_ACTIVE_PIXELS_PATH, 'wb') as fh:
                pickle.dump(self.active_pixels, fh, -1)
            message += "Active pixels saved; "

        if key_event_msg.char == "e":
            self.colors_lookup[self.colors_lookup == self.color] = 0
            message += "Erased color {}; ".format(str(self.color_name))

        if key_event_msg.char == "r":
            self.colors_lookup[:] = 0
            message += "Reset all colors"

        try:
            nr = int(key_event_msg.char)
            self.color = nr
            self.color_name = conf.COLORS.keys()[conf.COLORS.values().index(self.color)]
        except ValueError:
            pass

        self.current_message = message + "Selected color: {}; Brush size: {}, Noise: {}".format(self.color_name, self.brush_size, self.noise)
        rospy.loginfo(self.current_message)

    def publish_img(self, image, publisher):
        if image is None:
            image = np.zeros((512, 512, 3), np.uint8)
            cv2.putText(self.image, 'No image', (10, 500), cv2.FONT_HERSHEY_SIMPLEX, 4,
                        (255, 255, 255), 2, cv2.LINE_AA)

        msg = CompressedImage()
        msg.header.stamp = rospy.Time.now()
        msg.format = "jpeg"

        encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 50]
        result, encimg = cv2.imencode('.jpg', image, encode_param)
        msg.data = np.array(encimg).tostring()

        publisher.publish(msg)


if __name__ == '__main__':
    try:
        rospy.init_node("ximea_color_calibrator")
        rate = rospy.Rate(5)
        calibrator = XimeaColorCalibrator()

        while not rospy.is_shutdown():
            calibrator.spin()
            rate.sleep()

    except rospy.ROSInterruptException:
        print 'exception'
pass
