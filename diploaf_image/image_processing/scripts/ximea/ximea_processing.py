#!/usr/bin/env python

import cPickle as pickle
import rospy
import numpy as np
import image_processing.ximea_conf as conf
from image_processing.ximea_camera import XimeaCamera
from image_processing.msg import RawObjectPos
from image_processing.msg import RawObjectPositions
import image_processing.image_processing_utils as utils
from geometry_msgs.msg import Point
import cv2
import time
from sensor_msgs.msg import CompressedImage
import diploaf_robot.config_client as config_client
from diploaf_robot.msg import KeyValue

BLUE_TARGET = conf.COLORS["target_blue"]
MAGENTA_TARGET = conf.COLORS["target_magenta"]


class XimeaProcessing(config_client.ConfigClient, XimeaCamera):
    def __init__(self):
        config_client.ConfigClient.__init__(self)
        XimeaCamera.__init__(self)

        self.init_config_client()

        self.segmented_buffer = None
        self.opponent_target = BLUE_TARGET
        self.own_target_pos = RawObjectPos()
        self.opponent_target_pos = RawObjectPos()
        self.balls = []
        self.img = None
        self.colors_lookup = None
        self.image_shape = None
        self.field_mask = np.zeros((conf.HEIGHT, conf.WIDTH, 1))
        self.field_white_ball_mask = np.zeros((conf.HEIGHT, conf.WIDTH, 1))
        self.image_pub = rospy.Publisher("image_processing/ximea_debug_img/compressed", CompressedImage, queue_size=1)
        self.publisher = rospy.Publisher("image_processing/ximea_raw_object_positions", RawObjectPositions,
                                         queue_size=1)
        self.field_obstruction_pub = rospy.Publisher("image_processing/field_obstruction", Point,
                                                     queue_size=1)
        self.gui_publisher = rospy.Publisher("rqt_diploaf_dashboard/status", KeyValue, queue_size=1)
        self.update_target_color()
        self.rate_remaining = 0

        # conf
        self.ball_obstructed_ratio = rospy.get_param("ximea_ball_obstructed_ratio")
        self.field_obstructed_ratio = rospy.get_param("ximea_field_obstructed_ratio")

        rospy.Timer(rospy.Duration(1), self.monitoring_pub_callback)

    def monitoring_pub_callback(self, event):
        if self.config['monitoring'] and self.config['ximea_enabled']:
            self.publish_gui_status()

    def update_target_color(self):
        self.opponent_target = MAGENTA_TARGET if self.config['target_color'] == 0 else BLUE_TARGET

    def conf_callback(self):
        self.update_target_color()

    def init_cam(self):
        super(XimeaProcessing, self).init_cam()

        try:
            with open(conf.XIMEA_COLORS_CONF_PATH, 'rb') as fh:
                self.colors_lookup = pickle.load(fh)
                self.cam.setColors(self.colors_lookup)
        except IOError:
            rospy.logerr("XIMEA camera color calibration load error")
            self.run = False
            return

        try:
            with open(conf.XIMEA_ACTIVE_PIXELS_PATH, 'rb') as fh:
                active_pixels = pickle.load(fh)
                self.cam.setPixels(active_pixels)
        except IOError:
            rospy.logerr("XIMEA active pixels conf error")
            self.run = False
            return
        self.image_shape = self.cam.image().shape
        self.segmented_buffer = self.cam.getBuffer()

    def additional_cam_init(self):
        self.cam.setColorMinArea(conf.COLORS["balls"], rospy.get_param("ximea_ball_min_area"))
        self.cam.setColorMinArea(conf.COLORS["field"], 800)
        self.cam.setColorMinArea(conf.COLORS["target_blue"], rospy.get_param("ximea_blue_min_area"))
        self.cam.setColorMinArea(conf.COLORS["target_magenta"], rospy.get_param("ximea_magenta_min_area"))

    def spin(self):
        if not self.config['ximea_enabled']:
            return

        if not self.run:
            return

        self.reset_field_mask()
        self.cam.analyse()

        if self.config['debug_mode']:
            if self.config['ximea_night_vision']:
                self.img = np.zeros(self.image_shape)
                self.img[self.segmented_buffer == conf.COLORS['balls']] = np.array([0, 0, 255], dtype=np.uint8)
                self.img[self.segmented_buffer == conf.COLORS['target_blue']] = np.array([255, 0, 0], dtype=np.uint8)
                self.img[self.segmented_buffer == conf.COLORS['target_magenta']] = np.array([255, 0, 255],
                                                                                            dtype=np.uint8)
                self.img[self.segmented_buffer == conf.COLORS['field']] = np.array([0, 255, 0], dtype=np.uint8)
                self.img[self.segmented_buffer == conf.COLORS['black']] = np.array([100, 255, 100], dtype=np.uint8)
                self.img[self.segmented_buffer == conf.COLORS['white']] = np.array([255, 255, 255], dtype=np.uint8)
                self.img[self.segmented_buffer == conf.COLORS['other']] = np.array([0, 0, 0], dtype=np.uint8)
            else:
                self.img = self.cam.image()

        self.analyze_balls()
        self.analyze_targets(BLUE_TARGET)
        self.analyze_targets(MAGENTA_TARGET)
        self.analyze_field()

        self.publish_positions()
        self.publish_debug_img()

    def reset_field_mask(self):
        self.field_mask = np.zeros((conf.HEIGHT, conf.WIDTH, 1))
        self.field_mask[self.segmented_buffer == conf.COLORS['field']] = np.array([1], dtype=np.uint8)

        #self.field_white_ball_mask = np.copy(self.field_mask)
        #self.field_white_ball_mask[self.segmented_buffer == conf.COLORS['white']] = np.array([1], dtype=np.uint8)
        #self.field_white_ball_mask[self.segmented_buffer == conf.COLORS['balls']] = np.array([1], dtype=np.uint8)

    def publish_debug_img(self):
        if self.config['debug_mode']:
            if self.img is None:
                self.img = np.zeros((512, 512, 3), np.uint8)
                cv2.putText(self.img, 'No image', (10, 500), cv2.FONT_HERSHEY_SIMPLEX, 4,
                            (255, 255, 255), 2, cv2.LINE_AA)
            self.add_center_mark_to_img()
            msg = CompressedImage()
            msg.header.stamp = rospy.Time.now()
            msg.format = "jpeg"
            encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 50]
            result, encimg = cv2.imencode('.jpg', self.img, encode_param)
            msg.data = np.array(encimg).tostring()
            self.image_pub.publish(msg)

    def add_center_mark_to_img(self):
        cv2.circle(self.img, (conf.MID_X, conf.MID_Y), 4, [0, 255, 255], 4)

    def analyze_balls(self):
        blobs = self.cam.getBlobs(conf.COLORS["balls"])
        balls_locations = []

        if len(blobs) > 0:
            for ball_description in blobs:
                """
                blob[0] - distance (do not use)
                blob[1] - angle (do not use)
                blob[2] - area
                blob[3] - x_center
                blob[4] - y_center
                blob[5] - x_left 
                blob[6] - x_right
                blob[7] - y_top
                blob[8] - y_bottom
                """

                ratio = self.get_field_around_ball_ratio(ball_description)

                if self.config['debug_mode']:
                    x = ball_description[3]
                    y = ball_description[4]
                    cv2.putText(self.img, "{}".format(int(ratio)), (x, y), cv2.FONT_HERSHEY_COMPLEX_SMALL, 1.5,
                                (0, 0, 0), 2, cv2.LINE_AA)

                if ratio < self.ball_obstructed_ratio:
                    self.draw_center_point(ball_description, [0, 0, 255])
                    continue

                self.draw_center_point(ball_description, [0, 255, 0])
                balls_locations.append(self.convert_raw_pos(ball_description))

        self.balls = balls_locations

    def get_field_around_ball_ratio(self, ball_description):
        x = ball_description[3]
        width = ball_description[6] - ball_description[5]
        height = ball_description[8] - ball_description[7]
        y = ball_description[4]
        radius_expand = 15

        x_start = np.max([0, x - width - radius_expand])
        x_end = np.min([x + width + radius_expand, conf.WIDTH])
        y_start = np.max([0, y - height - radius_expand])
        y_end = np.min([y + height + radius_expand, conf.HEIGHT])
        cropped = self.field_mask[y_start:y_end, x_start:x_end]
        ratio = utils.get_color_coverage(cropped)

        if self.img is not None and self.config['debug_mode']:
            self.img[y_start:y_end, x_start:x_end] = [255, 100, 0]

        return ratio

    def analyze_targets(self, target):
        blobs = self.cam.getBlobs(target)

        if len(blobs) > 0:
            biggest_blob = blobs[0]
            target_pos = self.convert_raw_pos(biggest_blob)
            if self.opponent_target == target:
                self.opponent_target_pos = target_pos
            else:
                self.own_target_pos = target_pos

            if self.config['debug_mode']:
                color = [255, 0, 255] if target == MAGENTA_TARGET else [255, 0, 0]
                self.draw_bounding_box(biggest_blob, color)
        else:
            pos = RawObjectPos()
            pos.x = -1
            if self.opponent_target == target:
                self.opponent_target_pos = pos
            else:
                self.own_target_pos = pos

    def analyze_field(self):
        obstructed_directions = []

        # Up left
        x_start = 0
        x_end = conf.WIDTH / 2
        y_start = 0
        y_end = conf.HEIGHT / 2
        up_left_obstructed = self._analyze_field(x_start, x_end, y_start, y_end)
        if up_left_obstructed:
            obstructed_directions.append(Point(x_start, y_start, 0))

        # Up right
        x_start = conf.WIDTH / 2
        x_end = conf.WIDTH
        y_start = 0
        y_end = conf.HEIGHT / 2
        up_right_obstructed = self._analyze_field(x_start, x_end, y_start, y_end)
        if up_right_obstructed:
            obstructed_directions.append(Point(x_end, y_start, 0))

        # Down left
        x_start = 0
        x_end = conf.WIDTH / 2
        y_start = conf.HEIGHT / 2
        y_end = conf.HEIGHT
        down_left_obstructed = self._analyze_field(x_start, x_end, y_start, y_end)
        if down_left_obstructed:
            obstructed_directions.append(Point(x_start, y_end, 0))

        # Down right
        x_start = conf.WIDTH / 2
        x_end = conf.WIDTH
        y_start = conf.HEIGHT / 2
        y_end = conf.HEIGHT
        down_right_obstructed = self._analyze_field(x_start, x_end, y_start, y_end)
        if down_right_obstructed:
            obstructed_directions.append(Point(x_end, y_end, 0))

        for direction in obstructed_directions:
            self.field_obstruction_pub.publish(direction)

    def _analyze_field(self, x_start, x_end, y_start, y_end):
        cropped = self.field_mask[y_start:y_end, x_start:x_end]
        ratio = utils.get_color_coverage(cropped)

        if self.config['debug_mode'] and ratio < self.field_obstructed_ratio:
            cv2.rectangle(self.img, (x_start, y_start), (x_end, y_end), [0, 0, 255], 2)

        return ratio < self.field_obstructed_ratio

    @staticmethod
    def convert_raw_pos(blob):
        raw_object_pos = RawObjectPos()
        raw_object_pos.x = blob[3]
        raw_object_pos.y = blob[4]
        raw_object_pos.x_left = blob[5]
        raw_object_pos.x_right = blob[6]
        raw_object_pos.y_top = blob[7]
        raw_object_pos.y_bottom = blob[8]

        return raw_object_pos

    def draw_bounding_box(self, blob, color=[255, 255, 255]):
        if self.config['debug_mode'] and self.img is not None:
            self.img[blob[7]:blob[8], blob[5]:blob[5] + 2] = color  # left line
            self.img[blob[7]:blob[8], blob[6]:blob[6] + 2] = color  # right line
            self.img[blob[7]:blob[7] + 2, blob[5]:blob[6]] = color  # top line
            self.img[blob[8]:blob[8] + 2, blob[5]:blob[6]] = color  # bottom line

    def draw_center_point(self, blob, color=[255, 255, 255]):
        if self.config['debug_mode']:
            x = blob[3]
            y = blob[4]
            cv2.circle(self.img, (x, y), 6, color, 2)

    def get_object_positions(self):
        positions = RawObjectPositions()
        positions.OwnGatePos = self.own_target_pos
        positions.OpponentGatePos = self.opponent_target_pos
        positions.Balls = self.balls

        return positions

    def publish_positions(self):
        self.publisher.publish(self.get_object_positions())

    def publish_gui_status(self):
        msg = KeyValue()
        msg.MsgType = "ximea_processing"
        msg.MsgKey = "rate_remaining"
        msg.MsgValue = str(self.rate_remaining)
        self.gui_publisher.publish(msg)


if __name__ == '__main__':
    try:
        rospy.init_node("ximea_processing")
        rate = rospy.Rate(60)
        camera = XimeaProcessing()

        cam_open_try = 0
        while not rospy.is_shutdown():
            try:
                cam_open_try += 1
                camera.init_cam()
                break
            except ValueError:
                rospy.logerr_throttle(600, "Ximea camera missing")

            if not camera.run:
                if cam_open_try < 5:
                    rospy.logerr("Ximea camera could not be opened. Retry {}".format(cam_open_try))
                else:
                    rospy.logerr_throttle(60, "Ximea camera could not be opened. Retrying every 2 seconds...")
                time.sleep(2)

        while not rospy.is_shutdown():
            camera.spin()
            rate.sleep()
            camera.rate_remaining = rate.remaining().to_sec()


    except rospy.ROSInterruptException:
        pass
