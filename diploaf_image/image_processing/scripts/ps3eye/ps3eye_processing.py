#!/usr/bin/env python

import image_processing.front_cam_conf as conf
import image_processing.image_processing_utils as utils
import cPickle as pickle
import numpy as np
import segment
import subprocess
import time
from image_processing.msg import RawObjectPos
from image_processing.msg import RawObjectPositions
import cv2
import rospy
import rospkg
from sensor_msgs.msg import CompressedImage
import diploaf_robot.config_client as config_client
from sensor_msgs.msg import Image
from cv_bridge import CvBridge, CvBridgeError
from diploaf_robot.msg import KeyValue

MAGENTA = 0
BLUE = 1


class Cam(config_client.ConfigClient):
    def __init__(self):
        # init
        super(Cam, self).__init__()
        rospy.init_node("ps3eye_processing")
        self.init_config_client()
        self.running = False

        self.is_wide_angle_camera = rospy.get_param("/front_cam") == "wide_angle_cam"
        self.min_ball_area = rospy.get_param("/front_cam_min_ball_area")

        # empty objects
        self.goal = self.config['target_color']
        self.goals = [RawObjectPos(), RawObjectPos()]
        self.frame_balls = []

        # empty frames
        with open(conf.COLORS_CONF_PATH, 'rb') as fh:  # color lookup table
            self.colors_lookup = pickle.load(fh)
            segment.set_table(self.colors_lookup)  # load table to C module
        self.fragmented = np.zeros((conf.HEIGHT, conf.WIDTH), dtype=np.uint8)
        self.t_ball = np.zeros((conf.HEIGHT, conf.WIDTH), dtype=np.uint8)
        self.t_goal_y = np.zeros((conf.HEIGHT, conf.WIDTH), dtype=np.uint8)
        self.t_goal_b = np.zeros((conf.HEIGHT, conf.WIDTH), dtype=np.uint8)
        self.t_debug = np.zeros((conf.HEIGHT, conf.WIDTH, 3), dtype=np.uint8)
        self.field_mask = np.zeros((conf.HEIGHT, conf.WIDTH, 1))

        # ros publishers and subscribers
        self.debug_img_pub = rospy.Publisher("image_processing/ps3eye_debug/compressed", CompressedImage, queue_size=1)
        self.obj_publisher = rospy.Publisher("image_processing/ps3eye_raw_object_positions", RawObjectPositions,
                                             queue_size=1)
        self.image_pub = rospy.Publisher("image_processing/ps3eye_img", Image, queue_size=1)
        self.gui_publisher = rospy.Publisher("rqt_diploaf_dashboard/status", KeyValue, queue_size=1)
        rospy.Timer(rospy.Duration(1), self.monitoring_pub_callback)

        # init cam and image

        conf.apply_front_cam_conf()
        self.cap = cv2.VideoCapture(conf.CAM_ID)
        self.cap.set(cv2.CAP_PROP_FPS, conf.FPS)
        self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, conf.HEIGHT)
        self.cap.set(cv2.CAP_PROP_FRAME_WIDTH, conf.WIDTH)
        self.cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 0)
        self.bridge = CvBridge()
        self.cv_image = None

    def conf_callback(self):
        self.goal = self.config['target_color']

    def monitoring_pub_callback(self, event):
        if self.config['monitoring']:
            self.publish_gui_status()

    def publish_gui_status(self):
        msg = KeyValue()
        msg.MsgType = "ps3eye_processing"
        msg.MsgKey = "front_cam_running"
        msg.MsgValue = "alive: {}".format(self.running)
        self.gui_publisher.publish(msg)

    def analyze_balls(self, t_ball):
        if not self.is_wide_angle_camera:
            erode_kernel = np.ones((4, 4), np.uint8)
            t_ball = cv2.erode(t_ball, erode_kernel, iterations=1)
            dilate_kernel = np.ones((10, 10), np.uint8)
            t_ball = cv2.dilate(t_ball, dilate_kernel, iterations=1)

        img, contours, hierarchy = cv2.findContours(t_ball, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)

        self.frame_balls = []
        bounding_rectangles = []

        for contour in contours:
            s = cv2.contourArea(contour)

            if s < self.min_ball_area:
                continue

            rect = cv2.boundingRect(contour)
            x, y, w, h = self.get_corrected_bounding_rect(rect)

            bounding_rectangles.append(rect)

            ball = self.build_object_pos(w, h, x, y, s)
            self.frame_balls.append(ball)

            if self.config['debug_mode']:
                x_debug, y_debug, w_debug, h_debug = rect
                x_debug = x_debug + w_debug / 2
                y_debug = y_debug + h_debug / 2
                cv2.circle(self.t_debug, (x_debug, y_debug), max(1, w_debug / 2), [0, 0, 255], 2)
                y_top = y_debug + h_debug
                y_bottom = max(y_debug - h_debug, 0)
                cv2.circle(self.t_debug, (x_debug, y_top), 5, [255, 0, 255], 2)
                cv2.circle(self.t_debug, (x_debug, y_bottom), 5, [0, 255, 255], 2)

    def get_corrected_bounding_rect(self, rect):
        if self.is_wide_angle_camera:  # The camera is turned 90 degrees
            y, x, h, w = rect
            x = x + w / 2
            y = y + h / 2
            x = conf.HEIGHT - x  # The camera is flipped
        else:
            x, y, w, h = rect
            x = x + w / 2
            y = y + h / 2

        return x, y, w, h

    def merge_near_contours(self, rect1, rect2, max_distance):
        if self.distance(rect1, rect2) < max_distance:
            rects, weights = cv2.groupRectangles([rect1, rect2], 0, 999999)
            return rects
        return [rect1, rect2]

    def distance(self, rect1, rect2):
        p1 = np.array((rect1[0], rect1[1]))
        p2 = np.array((rect2[0], rect2[1]))
        return np.linalg.norm(p1 - p2)

    def analyze_goals(self, t_goal, goal_nr):
        erode_kernel = np.ones((3, 3), np.uint8)
        t_goal = cv2.erode(t_goal, erode_kernel, iterations=1)
        dilate_kernel = np.ones((5, 5), np.uint8)
        t_goal = cv2.dilate(t_goal, dilate_kernel, iterations=1)

        img, contours, hierarchy = cv2.findContours(t_goal, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        self.goals[goal_nr] = RawObjectPos()
        self.goals[goal_nr].x = -1
        s_max = 0
        original_rect = None
        for contour in contours:
            s = cv2.contourArea(contour)
            if s < 150:  # too small area
                continue
            if s > s_max:
                original_rect = cv2.boundingRect(contour)
                x, y, w, h = self.get_corrected_bounding_rect(original_rect)
                s_max = s
                goal = self.build_object_pos(w, h, x, y, s)
                self.goals[goal_nr] = goal

        self.add_debug_info(original_rect, goal_nr, contours)

    def add_debug_info(self, rect, goal_nr, contours):
        if self.config['debug_mode'] and rect is not None:
            x_d, y_d, w_d, h_d = rect
            top_left = (x_d, y_d)
            bottom_right = (x_d + (w_d), y_d + (h_d))
            color = [255, 0, 0] if goal_nr == BLUE else [0, 255, 255]
            cv2.rectangle(self.t_debug, top_left, bottom_right, color, 2)
            cv2.drawContours(self.t_debug, contours, -1, [0, 0, 0])

    def analyze_goal_obstructed(self):
        goal = self.goals[self.goal]


    @staticmethod
    def build_object_pos(width, height, x, y, area):
        object_pos = RawObjectPos()
        object_pos.width = width
        object_pos.height = height
        object_pos.x = x
        object_pos.y = y
        object_pos.y_top = y + height
        object_pos.y_bottom = max(y - height, 0)
        object_pos.area = 0

        return object_pos

    def analyze_frame(self):
        _, self.cv_image = self.cap.read()

        if self.cv_image is None or len(self.cv_image) < 1:
            raise CamDisconnectedException("Camera disconnected")

        segment.segment(self.cv_image, self.fragmented, self.t_ball, self.t_goal_y,
                        self.t_goal_b)

        self.reset_field_mask()
        self.analyze_balls(self.t_ball)
        self.analyze_goals(self.t_goal_y, MAGENTA)
        self.analyze_goals(self.t_goal_b, BLUE)
        self.analyze_goal_obstructed()

    def reset_field_mask(self):
        self.field_mask = np.zeros((conf.HEIGHT, conf.WIDTH, 1))
        self.field_mask[self.fragmented == conf.COLORS['field']] = np.array([1], dtype=np.uint8)

    def run(self):
        self.running = self.cap.isOpened()
        _, self.cv_image = self.cap.read()

        rate = rospy.Rate(conf.FPS)

        while not rospy.is_shutdown():
            try:
                self.spin_once()
            except (cv2.error, CamDisconnectedException) as e:
                rospy.logerr(e)
                self.running = False
                self.reinit_cam()
            finally:
                rate.sleep()

    def reinit_cam(self):
        self.cap.release()
        self.reset_usb()
        time.sleep(1)
        conf.apply_front_cam_conf()
        self.cap = cv2.VideoCapture(conf.CAM_ID)
        self.cap.set(cv2.CAP_PROP_FPS, conf.FPS)
        _, self.cv_image = self.cap.read()
        self.running = self.cap.isOpened()

    def reset_usb(self):
        hardware_module_package_path = rospkg.RosPack().get_path('hardware_module')
        usb_reset_script_location = "{}/misc/usbreset".format(hardware_module_package_path)

        for usb in subprocess.check_output(['lsusb']).split('\n')[:-1]:
            device_id = usb[23:32]
            if device_id == conf.DEVICE_ID:
                bus = usb[4:7]
                device_nr = usb[15:18]
                comm = 'sudo {} /dev/bus/usb/{}/{}'.format(usb_reset_script_location, bus, device_nr)
                rospy.loginfo("Reseting usb device: {}".format(subprocess.check_output(comm, shell=True)))

    def spin_once(self):
        self.publish_img_for_aruco()

        if not self.config['front_cam_enabled']:
            return

        self.analyze_frame()

        raw_object_positions = RawObjectPositions()
        raw_object_positions.OwnGatePos = self.goals[1 - self.goal]
        raw_object_positions.OpponentGatePos = self.goals[self.goal]
        raw_object_positions.Balls = self.frame_balls
        self.obj_publisher.publish(raw_object_positions)

        self.publish_debug_img()

    def publish_debug_img(self):
        if self.config['debug_mode'] and self.t_debug is not None and len(self.t_debug) > 0:
            msg = CompressedImage()
            msg.header.stamp = rospy.Time.now()
            msg.format = "jpeg"
            msg.data = np.array(cv2.imencode('.jpg', self.t_debug)[1]).tostring()
            self.debug_img_pub.publish(msg)
            self.t_debug = np.zeros((conf.WIDTH, conf.HEIGHT, 3), dtype=np.uint8)
            self.t_debug = self.cv_image

    def publish_img_for_aruco(self):
        if self.config['aruco_enabled']:
            self.publish_img()

    def publish_img(self):
        try:
            self.image_pub.publish(self.bridge.cv2_to_imgmsg(self.cv_image, encoding="rgb8"))
        except CvBridgeError as e:
            print(e)


class CamDisconnectedException(Exception):
    pass


if __name__ == '__main__':
    try:
        camera = Cam()
        camera.run()

    except rospy.ROSInterruptException:
        pass
