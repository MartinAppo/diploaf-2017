#!/usr/bin/env python

import cPickle as pickle
import errno
import numpy as np

import cv2
import os

import image_processing.front_cam_conf as conf


def nothing(x):
    pass


conf.apply_front_cam_conf()
cap = cv2.VideoCapture(conf.CAM_ID)
cap.set(cv2.CAP_PROP_FPS, conf.FPS)
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, conf.HEIGHT)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, conf.WIDTH)
cap.set(cv2.CAP_PROP_AUTO_EXPOSURE, 0)

cv2.namedWindow('image')
cv2.namedWindow('regular')
cv2.namedWindow('mask')
cv2.moveWindow('mask', 400, 0)
try:
    with open(conf.COLORS_CONF_PATH, 'rb') as fh:
        colors_lookup = pickle.load(fh)
except:
    colors_lookup = np.zeros(0x1000000, dtype=np.uint8)

cv2.createTrackbar('brush_size', 'image', 3, 10, nothing)
cv2.createTrackbar('noise', 'image', 1, 5, nothing)

mouse_x = 0
mouse_y = 0
brush_size = 1
noise = 1
p = 0
update_i = 0


def change_color():
    global update_i, noise, brush_size, mouse_x, mouse_y
    update_i -= 1
    ob = yuv[max(0, mouse_y - brush_size):min(conf.HEIGHT, mouse_y + brush_size + 1),
         max(0, mouse_x - brush_size):min(conf.WIDTH, mouse_x + brush_size + 1), :].reshape((-1, 3)).astype('int32')
    noises = xrange(-noise, noise + 1)
    for y in noises:
        for u in noises:
            for v in noises:
                colors_lookup[
                    ((ob[:, 0] + y) + (ob[:, 1] + u) * 0x100 + (ob[:, 2] + v) * 0x10000).clip(0, 0xffffff)] = p


# mouse callback function
def choose_color(event, x, y, flags, param):
    global update_i, noise, brush_size, mouse_x, mouse_y
    if event == cv2.EVENT_LBUTTONDOWN:
        mouse_x = x
        mouse_y = y
        brush_size = cv2.getTrackbarPos('brush_size', 'image')
        noise = cv2.getTrackbarPos('noise', 'image')
        update_i = 60
        change_color()


def open_or_create(filename):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    print "current dir path:{}".format(dir_path)
    if not os.path.exists(os.path.dirname(filename)):
        try:
            print "creating new location"
            os.makedirs(os.path.dirname(filename))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
    print "opening path"
    return open(filename, 'wb')


cv2.namedWindow('regular')
cv2.setMouseCallback('regular', choose_color)
cv2.setMouseCallback('mask', choose_color)

print("V2ljumiseks vajutada t2hte 'q', v22rtuste salvestamiseks 's', v22rtuste kustutamiseks 'e'")
print("Palli confimiseks 'r', magenta='y', sinine='b', v2ljak='g', valge='w', must='d', muu='o'")

i = 0
while (True):
    # Capture frame-by-frame
    _, yuv = cap.read()

    if update_i > 0:
        change_color()

    i += 1
    if i % 5 == 0:  # Display the resulting frame
        i = 0
        cv2.imshow('regular', cv2.cvtColor(yuv, cv2.COLOR_YUV2BGR)[:, :, ::-1])

        yuv = yuv.astype('uint32')
        fragmented = colors_lookup[yuv[:, :, 0] + yuv[:, :, 1] * 0x100 + yuv[:, :, 2] * 0x10000]
        frame = np.zeros(yuv.shape)
        frame[fragmented == conf.COLORS['balls']] = np.array([0, 0, 255], dtype=np.uint8)
        frame[fragmented == conf.COLORS['target_magenta']] = np.array([0, 255, 255], dtype=np.uint8)
        frame[fragmented == conf.COLORS['target_blue']] = np.array([255, 0, 0], dtype=np.uint8)
        frame[fragmented == conf.COLORS['field']] = np.array([0, 255, 0], dtype=np.uint8)
        frame[fragmented == conf.COLORS['white']] = np.array([255, 255, 255], dtype=np.uint8)
        frame[fragmented == conf.COLORS['black']] = np.array([255, 255, 0], dtype=np.uint8)
        cv2.imshow('mask', frame)

    k = cv2.waitKey(1) & 0xff

    if k == ord('q'):
        break
    elif k == ord('r'):
        print('balls')
        p = conf.COLORS['balls']
    elif k == ord('y'):
        print('magenta')
        p = conf.COLORS['target_magenta']
    elif k == ord('b'):
        print('blue gate')
        p = conf.COLORS['target_blue']
    elif k == ord('g'):
        print('field')
        p = conf.COLORS['field']
    elif k == ord('w'):
        print('white')
        p = conf.COLORS['white']
    elif k == ord('d'):
        print('black')
        p = conf.COLORS['black']
    elif k == ord('o'):
        print('everything else')
        p = 0
    elif k == ord('s'):
        with open_or_create(conf.COLORS_CONF_PATH) as fh:
            print('saving')
            pickle.dump(colors_lookup, fh, -1)
        print('saved')
    elif k == ord('e'):
        print('erased')
        colors_lookup[colors_lookup == p] = 0
# When everything done, release the capture
cap.release()
cv2.destroyAllWindows()
