#!/bin/bash

#log
exec 3>&1 4>&2
trap 'exec 2>&4 1>&3' 0 1 2 3
exec 1>/tmp/log 2>&1

sudo apt install -y tightvncserver autocutsel fluxbox expect

mkdir -p ~/.vnc

#vnc desktop env setup
echo '#!/bin/sh' >> ~/.vnc/xstartup
echo "autocutsel -fork" >> ~/.vnc/xstartup
echo "fluxbox &" >> ~/.vnc/xstartup
echo "sleep 2" >> ~/.vnc/xstartup
chmod 755 ~/.vnc/xstartup


#vnc
/usr/bin/expect <<EOF
spawn "/usr/bin/vncpasswd"
expect "Password:"
send "parool1\r"
expect "Verify:"
send "parool1\r"
expect "Would you like to enter a view-only password (y/n)?"
send "n\r"
expect eof
EOF

/usr/bin/vncserver :1 -geometry 1600x900

exit 0