#Robot remote control docs

robot1 ip:x, hostname:x, user:x, password:x  
robot2 ip: 10.42.0.1, hostname: robotiina, user: robotiina, password: robotiina

* map robot ip to robotiina in client hosts file
echo "10.42.0.1 robotiina" >> /etc/hosts



##SSH

1. on robot  
  1.1 install ssh server  
sudo apt install openssh-server

  1.2 add setup.bash to .bashrc file  
echo "source ~/CLionProjects/diploaf_ws/devel/setup.bash" >> ~/.bashrc

2. on client  
  2.1 windows - run ssh-robotX.bat OR  
start putty.exe -L 5901:localhost:5901 -ssh robotiina -pw robotiina 

  2.2 linux - run ssh-robotX.sh OR  
ssh -L 5901:localhost:5901 robotiina
	
	
	
##ROS gui

1. on robot  
  1.1 set master ip  
export ROS_MASTER_URI=http://localhost:11311  
echo "export ROS_MASTER_URI=http://localhost:11311" >> ~/.bashrc  

  1.2 start ros  
cd ~/diploaf_ws/src/  
roslaunch diploaf_robot robot.launch

2. on client
  2.1 set master ip  
export ROS_MASTER_URI=http://robotiina:11311  
echo "export ROS_MASTER_URI=http://robotiina:11311" >> ~/.bashrc

  2.2 start ros gui  
cd ~/diploaf_ws/src/  
roslaunch diploaf_robot gui.launch



##VNC

1. on robot  
  1.1 install vncserver - run vnc-setup.sh from /misc
  
  1.2 start vncserver on boot - copy /misc/setup/vncserver content to /etc/init.d/vncserver  
sudo update-rc.d vncserver defaults

  1.3 replace fluxbox menu - copy /misc/setup/menu to ~/.fluxbox/menu

2. on client  
  2.1 install vncviewer windows  
    2.1.1 https://www.tightvnc.com/download/2.8.8/tightvnc-2.8.8-gpl-setup-64bit.msi  
	2.1.2 run vnc-robotX-win.vnc OR vncviewer.exe where ip=localhost:5901, password=robotiina
	
  2.2 install vncviewer linux and run vnc-robot.sh  
sudo apt install vncviewer



##HOTSPOT  
https://askubuntu.com/questions/318973/how-do-i-create-a-wifi-hotspot-sharing-wireless-internet-connection-single-adap

1. add repository
sudo add-apt-repository ppa:nilarimogard/webupd8

2. install ap-hostspot
apt update && apt install ap-hotspot

3. start/setup ap-hotspot (commands: start stop restart Configure)
sudo ap-hotspot start
(wlan1 is robot wifi, wlan2 is usb wifi)


