from logic_module.msg import State


class StateFactory(object):
    STOPPED = "STOPPED"
    IDLE = "IDLE"
    GOING_TO_BALL = "GOING_TO_BALL"
    REACHED_THE_BALL = "REACHED_THE_BALL"
    THROWING = "THROWING"
    THROWING_WITH_MISSING_BALL = "THROWING_WITH_MISSING_BALL"
    SCANNING_FIELD = "SCANNING_FIELD"
    GOING_TO_CENTER = "GOING_TO_CENTER"

    GRABBING_BALL = "GRABBING_BALL"
    GRABBED_BALL = "GRABBED_BALL"
    GOING_TO_BETTER_THROW_DIST = "GOING_TO_BETTER_THROW_DIST"

    @staticmethod
    def getStoppedMsg():
        state = State()
        state.name = StateFactory.STOPPED
        state.value = -1
        return state

    @staticmethod
    def getIdleMsg():
        state = State()
        state.name = StateFactory.IDLE
        state.value = -1
        return state

    @staticmethod
    def getGoingToBallMsg(ball_id):
        state = State()
        state.name = StateFactory.GOING_TO_BALL
        state.value = ball_id
        return state

    @staticmethod
    def getReachedTheBallMsg():
        state = State()
        state.name = StateFactory.REACHED_THE_BALL
        state.value = -1
        return state

    @staticmethod
    def getThrowingMsg():
        state = State()
        state.name = StateFactory.THROWING
        state.value = -1
        return state

    @staticmethod
    def getThrowingWithMissingBallMsg():
        state = State()
        state.name = StateFactory.THROWING_WITH_MISSING_BALL
        state.value = 100  # TODO Make configurable
        return state

    @staticmethod
    def getGrabbingBallMsg():
        state = State()
        state.name = StateFactory.GRABBING_BALL
        state.value = 100
        return state

    @staticmethod
    def getGrabbedBallMsg():
        state = State()
        state.name = StateFactory.GRABBED_BALL
        return state

    @staticmethod
    def getGoingToBetterThrowDistMsg():
        state = State()
        state.name = StateFactory.GOING_TO_BETTER_THROW_DIST
        return state

    @staticmethod
    def getScanningFieldMsg():
        state = State()
        state.name = StateFactory.SCANNING_FIELD
        state.value = -1
        return state

    @staticmethod
    def getGoingToCenterMsg():
        state = State()
        state.name = StateFactory.GOING_TO_CENTER
        state.value = -1
        return state
