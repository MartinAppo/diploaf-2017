#! /usr/bin/env python
import diploaf_robot.config_client as config_client
import rospy
import time
from diploaf_robot.msg import KeyValue
from logic_module.msg import CommandResult
from logic_module.srv import CommandCall


class GameLogicV2(config_client.ConfigClient):
    def __init__(self):
        config_client.ConfigClient.__init__(self)
        rospy.init_node("game_logic", anonymous=True)
        self.init_config_client()

        self.current_state = None

        self.call_approach_ball = rospy.ServiceProxy('approach_ball', CommandCall)
        self.call_go_to_ball = rospy.ServiceProxy('go_to_ball', CommandCall)
        self.call_idle = rospy.ServiceProxy('idle', CommandCall)
        self.call_throw = rospy.ServiceProxy('throw', CommandCall)
        self.call_scan_field = rospy.ServiceProxy('scan_field', CommandCall)
        self.call_go_to_better_throw_dist = rospy.ServiceProxy('go_to_better_throw_dist', CommandCall)

        self.enabled = self.config['game_start']

        self.monitoring = self.config['monitoring']
        self.state_statistics = {}  # Dict: Key -> State name, Value -> [total_duration, nr_of_times_happened]
        self.last_state_switch_time = time.time()
        self.gui_publisher = rospy.Publisher("rqt_diploaf_dashboard/status", KeyValue, queue_size=1)

        self.previous_game_start = self.config['game_start']

    def start(self):
        self.current_state = get_state_instance(IdleState)

    def spin_once(self):
        self.current_state.execute()

        if not self.config['game_start']:
            new_state = get_state_instance(IdleState)
        else:
            new_state = self.current_state.get_next_state()

        self.change_state(new_state)

    def change_state(self, new_state):
        if new_state.name != self.current_state.name:
            rospy.loginfo("New state transition: {} ---> {}".format(self.current_state.name, new_state.name))
            self.current_state = new_state

    def set_game_started_conf(self):
        self.set_config('ximea_enabled', 1)
        self.set_config('front_cam_enabled', 1)
        self.set_config("ball_scanner_cam", 0)
        self.set_config('debug_mode', 0)
        self.set_config('launcher_enabled', 0)
        self.set_config('lock_direction', 1)
        self.set_config('launcher_fixed_speed', 0)

    def stop_game(self):
        self.set_config("game_start", 0)

    def publish_monitoring_info(self, state):
        if self.state.name != state.name:
            if self.state.name not in self.state_statistics:
                self.state_statistics[self.state.name] = [time.time() - self.last_state_switch_time, 1]
            else:
                self.state_statistics[self.state.name][0] += time.time() - self.last_state_switch_time
                self.state_statistics[self.state.name][1] += 1
            self.last_state_switch_time = time.time()
        msg = KeyValue()
        msg.MsgType = "state_statistics"
        msg.MsgKey = "state_statistics"
        msg.MsgValue = ""
        for name, info in self.state_statistics.iteritems():
            msg.MsgValue += name + " -> " + str(info[0] / info[1]) + " (Total duration: " + str(
                info[0]) + ", Nr of times: " + str(info[1]) + ")\n"
        self.gui_publisher.publish(msg)
        msg = KeyValue()
        msg.MsgType = "current_state"
        msg.MsgKey = "current_state"
        msg.MsgValue = self.state.name
        self.gui_publisher.publish(msg)


###############
# Game states #
###############

game_state_map = {}


def get_state_instance(state_class):
    return game_state_map[state_class.name]


class GameState:
    name = ""

    def __init__(self, game_logic_instance):
        self.game_logic = game_logic_instance
        self.last_result = None

    def execute(self):
        # implement in superclass
        pass

    def get_next_state(self):
        # implement in superclass
        pass

    def call_service(self, service_call):
        rospy.wait_for_service(service_call.resolved_name)
        return service_call().result.type


class IdleState(GameState):
    name = "idle"

    def __init__(self, game_logic_instance):
        GameState.__init__(self, game_logic_instance)

    def execute(self):
        self.last_result = self.call_service(self.game_logic.call_idle)

    # Get out of idle state only when game_start parameter is set to true
    def get_next_state(self):
        if self.game_logic.config['game_start']:
            return get_state_instance(ScanningFieldState)
        else:
            return self


class GoingToBallState(GameState):
    name = "going_to_ball"

    def __init__(self, game_logic_instance):
        GameState.__init__(self, game_logic_instance)

    def execute(self):
        self.last_result = self.game_logic.call_go_to_ball().result.type

    # If ball is lost, then start scanning field
    def get_next_state(self):
        if self.last_result == CommandResult.TYPE_BALL_NOT_FOUND or self.last_result == CommandResult.TYPE_OBJECT_LOST:
            return get_state_instance(ScanningFieldState)
        if self.last_result == CommandResult.TYPE_SUCCESS:
            return get_state_instance(ThrowingState)


class DebugState(GameState):
    name = "debug_state"

    def __init__(self, game_logic_instance):
        GameState.__init__(self, game_logic_instance)

    def execute(self):
        rospy.loginfo_throttle(1, "in debug state")

    def get_next_state(self):
        return get_state_instance(DebugState)


class ScanningFieldState(GameState):
    name = "scanning_field"

    def __init__(self, game_logic_instance):
        GameState.__init__(self, game_logic_instance)

    def execute(self):
        self.last_result = self.game_logic.call_scan_field().result.type

    # If ball is found during field scan, then go to ball
    def get_next_state(self):
        if self.last_result == CommandResult.TYPE_SUCCESS:
            return get_state_instance(GoingToBallState)
        else:
            return self


class ApproachingBallState(GameState):
    name = "approaching_ball"

    def __init__(self, game_logic_instance):
        GameState.__init__(self, game_logic_instance)

    def execute(self):
        self.last_result = self.game_logic.call_approach_ball().result.type

    # If approaching ball is successful, go to ball
    def get_next_state(self):
        if self.last_result == CommandResult.TYPE_SUCCESS:
            return get_state_instance(GoingToBallState)
        elif self.last_result == CommandResult.TYPE_BALL_NOT_FOUND or self.last_result == CommandResult.TYPE_OBJECT_LOST:
            return get_state_instance(ScanningFieldState)
        else:
            return self


class ThrowingState(GameState):
    name = "throwing"

    def __init__(self, game_logic_instance):
        GameState.__init__(self, game_logic_instance)

    def execute(self):
        self.last_result = self.game_logic.call_throw().result.type

    # If ball throw is success, find another ball
    def get_next_state(self):
        if self.last_result == CommandResult.TYPE_SUCCESS:
            return get_state_instance(ApproachingBallState)
        elif self.last_result == CommandResult.TYPE_OBJECT_LOST or self.last_result == CommandResult.TYPE_BALL_NOT_FOUND:
            rospy.loginfo("approach: ball not found")
            return get_state_instance(ScanningFieldState)
        else:
            return self


if __name__ == '__main__':
    try:
        game_logic = GameLogicV2()

        # IMPORTANT you must add every new state in game_state_map
        # keep only single instance of every state
        game_state_map[IdleState.name] = IdleState(game_logic)
        game_state_map[GoingToBallState.name] = GoingToBallState(game_logic)
        game_state_map[ApproachingBallState.name] = ApproachingBallState(game_logic)
        game_state_map[ScanningFieldState.name] = ScanningFieldState(game_logic)
        game_state_map[ThrowingState.name] = ThrowingState(game_logic)
        game_state_map[DebugState.name] = DebugState(game_logic)

        game_logic.start()

        rate = rospy.Rate(60)

        while not rospy.is_shutdown():
            game_logic.spin_once()
            rate.sleep()

    except rospy.ROSInterruptException:
        pass
