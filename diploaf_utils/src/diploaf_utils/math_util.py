from math import *
import numpy as np


class Vector():
    def __init__(self):
        self.x = 0.0
        self.y = 0.0

    def get_length(self):
        return vector_to_direction_and_length(self)['len']

    def get_direction(self):
        return vector_to_direction_and_length(self)['dir']

    def get_direction_180(self):
        dir_and_len = vector_to_direction_and_length(self)
        direction = dir_and_len['dir']
        dir_180 = 360 - direction if direction > 180 else direction
        return dir_180


def cart2pol(x, y):
    distance = np.sqrt(x ** 2.0 + y ** 2.0)
    angle = np.arctan2(y, x)
    return distance, angle


def pol2cart(distance, angle):
    x = distance * np.sin(np.radians(angle))
    y = distance * np.cos(np.radians(angle))
    return round(x, 3), round(y, 3)


def vector_to_direction_and_length(vector):
    x = vector.x
    y = vector.y
    length = sqrt(pow(x, 2.0) + pow(y, 2.0))
    if y != 0:
        direction = atan2(x, y)
        if direction < 0:
            direction += 2.0 * pi
    elif x >= 0:
        direction = pi / 2.0
    else:
        direction = 1.5 * pi
    return {'len': length, 'dir': degrees(direction)}


def direction_and_length_to_vector(direction, length):
    vector = Vector()
    vector.x = round(sin(radians(direction)) * length, 3)
    vector.y = round(cos(radians(direction)) * length, 3)
    return vector


def change_vector_length(vector, len):
    dir_and_len = vector_to_direction_and_length(vector)
    return direction_and_length_to_vector(dir_and_len['dir'], len)


def change_vector_length_and_dir(vector, len, dir):
    dir_and_len = vector_to_direction_and_length(vector)
    return direction_and_length_to_vector(dir_and_len['dir'] + dir, len)


def limit_vector_length(vector, limit_len):
    vector_len = vector_to_direction_and_length(vector)['len']
    if vector_len > limit_len:
        return change_vector_length(vector, limit_len)
    else:
        return vector


def limit_min_vector_length(vector, min_len):
    vector_len = vector_to_direction_and_length(vector)['len']
    if vector_len < min_len:
        return change_vector_length(vector, min_len)
    else:
        return vector


def get_vector_between_two_points(x1, y1, x2, y2):
    vector = Vector()
    vector.x = x2 - x1
    vector.y = y2 - y1
    return vector


def get_vector_between_two_vectors(vector1, vector2):
    return get_vector_between_two_points(vector1.x, vector1.y, vector2.x, vector2.y)


def get_distance_between_two_vectors(vector1, vector2):
    vector = get_vector_between_two_vectors(vector1, vector2)
    return vector_to_direction_and_length(vector)['len']


def sum_vectors(vector1, vector2):
    vector = Vector()
    vector.x = vector1.x + vector2.x
    vector.y = vector1.y + vector2.y
    return vector


def get_opposite_vector(vector):
    opposite_vector = Vector()
    opposite_vector.x = vector.x * - 1.0
    opposite_vector.y = vector.y * - 1.0
    return opposite_vector
