import os
import rospy
import rospkg

from qt_gui.plugin import Plugin
from python_qt_binding import loadUi
from python_qt_binding.QtWidgets import QWidget
from hardware_module.msg import Launcher
from diploaf_robot.msg import KeyValue
from time import time


class DiploafDashboard(Plugin):
    def __init__(self, context):
        super(DiploafDashboard, self).__init__(context)
        self.setObjectName('DiploafDashboard')

        from argparse import ArgumentParser
        parser = ArgumentParser()
        parser.add_argument("-q", "--quiet", action="store_true",
                            dest="quiet",
                            help="Put plugin in silent mode")
        args, unknowns = parser.parse_known_args(context.argv())
        if not args.quiet:
            print 'arguments: ', args
            print 'unknowns: ', unknowns

        self._widget = QWidget()
        ui_file = os.path.join(rospkg.RosPack().get_path('rqt_diploaf_dashboard'), 'resources', 'diploaf_dashboard.ui')
        loadUi(ui_file, self._widget)
        self._widget.setObjectName('DiploafDashboard')

        if context.serial_number() > 1:
            self._widget.setWindowTitle(self._widget.windowTitle() + (' (%d)' % context.serial_number()))

        context.add_widget(self._widget)

        self.launcher_msg = Launcher()
        self.launcher_msg.servoPos = 800
        self.launcher_msg.motorSpeed = 0
        self.msg_types = {}

        self.launcher_pub = None
        self.launcher_sub = None
        self.gui_sub = None
        self.init_ros()
        self.init_signals()
        self.update_ui_values()

        rospy.Timer(rospy.Duration(1), self.update_monitoring)

    def update_monitoring(self, arg):
        if rospy.is_shutdown():
            return

        monitoring_str = ""
        cur_time = time()
        for key, value in self.msg_types.iteritems():
            # Remove value, if outdated
            last_updated = value[1]
            if cur_time - last_updated > 2:
                value[0] = "None"

            monitoring_str += "{}: {}\n".format(key, value[0])

        self._widget.monitoring.setText(monitoring_str)

    def init_ros(self):
        self.launcher_pub = rospy.Publisher("hardware_module/launcher", Launcher, queue_size=1)
        self.launcher_sub = rospy.Subscriber("hardware_module/launcher", Launcher, self.launcher_callback)
        self.gui_sub = rospy.Subscriber("rqt_diploaf_dashboard/status", KeyValue, self.update_gui_status_callback)

    def launcher_callback(self, launcher_msg):
        self.launcher_msg = launcher_msg
        self.update_ui_values()

    def init_signals(self):
        self._widget.servo_slider.valueChanged.connect(self.servo_slider_changed)
        self._widget.motor_slider.valueChanged.connect(self.motor_slider_changed)

    def servo_slider_changed(self):
        self.launcher_msg.servoPos = self._widget.servo_slider.value()
        self._widget.current_servo_pos.setText(str(self.launcher_msg.servoPos))
        self.launcher_pub.publish(self.launcher_msg)

    def motor_slider_changed(self):
        self.launcher_msg.motorSpeed = self._widget.motor_slider.value()
        self._widget.current_motor_speed.setText(str(self.launcher_msg.motorSpeed))
        self.launcher_pub.publish(self.launcher_msg)

    def update_ui_values(self):
        self._widget.motor_slider.setValue(self.launcher_msg.motorSpeed)
        self._widget.servo_slider.setValue(self.launcher_msg.servoPos)

    def update_gui_status_callback(self, msg):
        self.msg_types[msg.MsgType] = [msg.MsgValue, time()]

    def shutdown_plugin(self):
        self.launcher_pub.unregister()
        self.launcher_sub.unregister()

    def save_settings(self, plugin_settings, instance_settings):
        # TODO save intrinsic configuration, usually using:
        # instance_settings.set_value(k, v)
        pass

    def restore_settings(self, plugin_settings, instance_settings):
        # TODO restore intrinsic configuration, usually using:
        # v = instance_settings.value(k)
        pass

        # def trigger_configuration(self):
        # Comment in to signal that the plugin has a way to configure
        # This will enable a setting button (gear icon) in each dock widget title bar
        # Usually used to open a modal configuration dialog
