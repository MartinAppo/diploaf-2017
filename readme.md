## Setup

### Installing ros
*ROS Kinetic requires ubuntu 16.04*

1. Install ROS Kinetic following this tutorial: http://wiki.ros.org/kinetic/Installation/Ubuntu ros-kinetic-desktop-full is reccomended.

### Set up workspace

1. `~$ mkdir diploaf_ws`

1. `~/diploaf_ws$ git clone https://MartinAppo@bitbucket.org/MartinAppo/diploaf-2017.git src`

1. `~/diploaf_ws$ catkin_make`

### Download dependencies and make project

1. `~$ sudo apt-get install python-pip`

1. `~/diploaf_ws$ rosdep install --from-paths --ignore-src .`

1. `~/diploaf_ws$ source devel/setup.bash`

### Install python segment module: 

1. `~/diploaf_ws/diploaf_image/image_processing/external/lib/segment_module$ sudo python setup.py install`

### Build and install Opencv

1. `$ sudo apt-get install python-dev python-numpy build-essential cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev`

1. `~$ git clone https://github.com/opencv/opencv.git`

1. `~/opencv$ git checkout 3.4`

1. `~$ git clone https://github.com/opencv/opencv_contrib.git`

1. `~/opencv_contrib$ git checkout 3.4`

1. `~/opencv$ mkdir release`

1. `~/opencv/release$ cmake -DOPENCV_EXTRA_MODULES_PATH=../../opencv_contrib/modules/aruco -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local ../`

1. `~/opencv/release$ make -j7`

1. `~/opencv/release$ sudo make install`

### Install Ximea API

1. `$ wget http://www.ximea.com/downloads/recent/XIMEA_Linux_SP.tgz`

1. `$ tar xzf XIMEA_Linux_SP.tgz`

1. `$ cd package`

1. `~/package$ ./install -cam_usb30`

### To give ximea more resources in order to work, add following line to /etc/rc.local file and restart:

1. echo 0 > /sys/module/usbcore/parameters/usbfs_memory_mb

### PyXiQ

1. `~/diploaf_ws/src/diploaf_image/image_processing/external/lib/pyXiQ$ sudo python setup.py install`

### Realsense installation

1. https://github.com/IntelRealSense/librealsense/blob/master/doc/distribution_linux.md#installing-the-packages

1. `$ pip install --user pyrealsense2`

### Other dependencies

1. `$ pip install --user pid_controller`

*Using Clion is reccomended for developing.*

 * Install Clion, import project from sources and choose python2.7 interpreter from settings
