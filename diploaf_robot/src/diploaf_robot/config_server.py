from dynamic_reconfigure.server import Server
from diploaf_robot.cfg import DiploafConfig


class ConfigServer(object):
    def __init__(self):
        self.conf_srv = None
        self.config = None

    def init_config_server(self):
        Server(DiploafConfig, self.conf_callback)

    def conf_callback(self, config, level):
        self.config = config
        return config
