import dynamic_reconfigure.client
import threading
import rospy
from rospy import ServiceException


class ConfigClient(object):
    def __init__(self):
        self.client = None
        self.config = None

    def init_config_client(self):
        self.client = dynamic_reconfigure.client.Client("config_server", timeout=30,
                                                        config_callback=self.conf_callback_)
        self.update_conf()

    def conf_callback_(self, config):
        self.config = config
        self.conf_callback()
        return config

    # Override if needed
    def conf_callback(self):
        pass

    def set_config(self, param, value):
        self.config[param] = value
        conf_updated = False
        while not conf_updated:
            try:
                self.client.update_configuration({param: value})
                conf_updated = True
            except ServiceException:
                rospy.logwarn("Could not change conf: {}".format({param: value}))

    def update_conf(self):
        if not rospy.is_shutdown():
            try:
                self.client.update_configuration({})
            except ServiceException:
                pass
