#!/usr/bin/env python
import rospy
import diploaf_robot.config_server as config_server


class ConfigServer(config_server.ConfigServer):
    def __init__(self):
        super(ConfigServer, self).__init__()
        rospy.init_node("config_server", anonymous=True)
        rospy.loginfo("Config server started")
        self.init_config_server()


if __name__ == '__main__':
    try:
        ConfigServer()
        rate = rospy.Rate(30)
        rospy.spin()
    except rospy.ROSInterruptException:
        pass
