#include <ros/ros.h>

#include <dynamic_reconfigure/client.h>
#include <dynamic_reconfigure/ConfigDescription.h>
#include <dynamic_reconfigure/Reconfigure.h>
#include <diploaf_robot/DiploafConfig.h>


class ConfigClient : public dynamic_reconfigure::Client<diploaf_robot::DiploafConfig> {
public:
    ConfigClient() :
            dynamic_reconfigure::Client<diploaf_robot::DiploafConfig>("/config_server",
                                                                      boost::bind(&ConfigClient::callback, this, _1)) {
    }

    void callback(const diploaf_robot::DiploafConfig &config);

protected:
    diploaf_robot::DiploafConfig config;

private:

};