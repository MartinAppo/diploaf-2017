import serial
import time
import sys
import math
import signal

servo_min = 1000.0
servo_max = 1400.0

motor_min = 1050.0
motor_max = 1450.0

servo_postions = 3
speed_positions = 2

servo_values = [800]
motor_values = [1050, 1100, 1150, 1200, 1250, 1300, 1350, 1400, 1450]
measurments = 3

servo_default = 1000
motor_default = 800

connection = None


#if sys.platform.startswith('win'):
#    ports = ['COM%s' % (i + 1) for i in range(256)]

connection_opened = False

ports=["COM4"]

for port in ports:
    try:
        while not connection_opened:
            print("Attempting conn on port: " + port)
            connection = serial.Serial(port, baudrate=115200, timeout=0.8, dsrdtr=False)
            connection_opened = connection.isOpen()
            time.sleep(0.5)
        connection.flush()
    except:
        continue

def write(comm):
    global connection
    #print("writing: " + comm)
    if connection is not None:
        try:
            connection.write((comm + '\n').encode('utf-8'))
        except(e):
            print('mainboard: err write ' + comm)

def servo(value):
    write("v{}".format(value))

def launch_motor(value):
    write("d{}".format(value))

def close():
    global connection
    if connection is not None and connection.isOpen():  # close coil
        try:
            connection.close()
            print('mainboard: connection closed')
        except:
            print('mainboard: err connection close')
        connection = None


def quit_all():
    close();

def is_connected():
    if connection is not None and connection.isOpen():
        return True
    return False


if is_connected():
    results = {}
    results_str = ""

    for servo_range in servo_values:

    #for servo_i in range(servo_postions + 1):
    #    servo_range = int(math.ceil((servo_max - servo_min)/servo_postions) * (servo_i) + servo_min)
        
        for motor_range in motor_values:
        
        #for motor_i in range(speed_positions + 1):   
        #    motor_range = int(math.ceil((motor_max - motor_min)/speed_positions) * (motor_i) + motor_min)
            servo(servo_range)
            launch_motor(motor_range)
            
            dist = []
            for measurment in range(measurments):
                dist_temp = None
                while(True):
                    if(dist_temp == None or type(dist_temp) != float):
                        try:
                            dist_temp = float(input("Servo at: {} ; Speed at: {} \nWhat was the distance?".format(servo_range, motor_range)))
                            dist.append(dist_temp)
                            break
                        except:
                            print("Faulty input. Try again!")
                            
            results[(servo_range, motor_range)] = dist
            results_str += str(servo_range) + ", " + str(motor_range) + ", " + str(dist) + "\n"

    servo(servo_default)
    launch_motor(motor_default)
    close()
    
    print(results)
    print(results_str)
